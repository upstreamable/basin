<?php

namespace Basin\Recipes\Itself\Commands;

use Gitlab\Client;
use Gitlab\Api\Packages;
use Gitlab\Api\Tags;
use Robo\Tasks;

/**
 * Helper commands to release packages.
 *
 * @see http://robo.li/
 */
class ReleaseCommands extends Tasks
{
    protected const SUPPORTED_PACKAGE_TYPES = ['deb', 'rpm', 'apk', 'bin'];
    protected const BUILD_DIR = 'app/docs/static';

    /**
     * Release packages for all supported formats.
     *
     * @command release:publish-all-packages
     */
    public function publishAllPackages($version)
    {
        foreach (self::SUPPORTED_PACKAGE_TYPES as $packageType) {
            $this->publishPackage($version, $packageType);
        }
    }

    /**
     * Publish a package file to the generic registry.
     *
     * @command release:publish-package
     */
    public function publishPackage($version, $type = 'deb')
    {
        $client = $this->getClient();
        if (!$client) {
            return;
        }

        $projectId = getenv('PACKAGE_VENDOR') . '/basin';
        $packageName = $type === 'bin' ? 'wrapper_script' : $type;
        $packages = new Packages($client);
        $packages->addGenericFile($projectId, $packageName, $version, $this->getPackagePathFromType($version, $type));
    }

    /**
     * Create a release from a tag.
     *
     * @command release:tag
     */
    public function release($version)
    {
        $client = $this->getClient();
        if (!$client) {
            return;
        }

        $projectId = getenv('PACKAGE_VENDOR') . '/basin';

        $tags = new Tags($client);
        $links = $this->getGenericPackageLinks($version, $projectId);
        $tags->createRelease($projectId, $version, ['assets' => [
            'links' => $links,
        ]]);
    }

    protected function getGenericPackageLinks($version, $projectId)
    {
        $links = [];
        foreach (self::SUPPORTED_PACKAGE_TYPES as $type) {
            $packageName = $type === 'bin' ? 'wrapper_script' : $type;
            $filepathExtension = $type === 'bin' ? '' : ('.' . $type);

            $links[] = [
                'name' => $packageName,
                'url' =>
                    'https://gitlab.com/api/v4/projects/' .
                    \urlencode($projectId) .
                    '/packages/generic/' .
                    $packageName . '/' .
                    $version . '/' .
                    \basename($this->getPackagePathFromType($version, $type)),
                'filepath' => '/basin'  . $filepathExtension,
                'link_type' => $type === 'bin' ? 'other' : 'package',
            ];
        }
        return $links;
    }

    protected function getClient()
    {
        $config = $this->getContainer()->get('config');
        $client = new Client();
        // This part is used when running as part of CI.
        if (getenv('CI_JOB_TOKEN')) {
            $client->authenticate(getenv('CI_JOB_TOKEN'), Client::AUTH_HTTP_JOB_TOKEN);
        } elseif ($token = $config->get('gitlab.token')) {
            $client->authenticate($token, Client::AUTH_HTTP_TOKEN);
        } else {
            $this->say('Authentication with gitlab api is required.');
            return false;
        }
        return $client;
    }

    protected function getPackagePathFromType($version, $type)
    {
        $packageTypeMap = [
            'deb' => 'basin_' . $version . '_all.deb',
            'rpm' => 'basin-' . $version . '-1.noarch.rpm',
            'apk' => 'basin_' . $version . '_all.apk',
            'bin' => 'bin/' . $version . '/basin',
        ];
        return self::BUILD_DIR . '/' . $packageTypeMap[$type];
    }
}

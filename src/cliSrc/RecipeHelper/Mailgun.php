<?php

namespace Basin\RecipeHelper;

trait Mailgun
{
    protected function addMailgun(array $dockerCompose, $config)
    {
        $consoleIO = $this->getContainer()->get('consoleio');

        $activeEnvironment = $config->get('environment.active');
        $enabled = $config->get('environment.' . $activeEnvironment . '.mail.mailgun.enabled');

        $key = $config->get('mail.mailgun.key');
        $domain = $config->get('mail.mailgun.domain');
        $region = $config->get('mail.mailgun.region');

        if (!$enabled) {
            return $dockerCompose;
        }
        $dockerCompose['services']['web']['environment']['MAILGUN_API_KEY'] = $key;
        $dockerCompose['services']['web']['environment']['MAILGUN_DOMAIN'] = $domain;
        if ($region) {
            $dockerCompose['services']['web']['environment']['MAILGUN_REGION'] = $region;
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MAILGUN_REGION';
        }

        if (isset($dockerCompose['services']['web']['environment']['MAILGUN_API_KEY'])) {
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MAILGUN_API_KEY';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MAILGUN_DOMAIN';
        }

        return $dockerCompose;
    }
}

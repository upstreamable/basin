<?php

namespace Basin\Recipes\WordpressVolume\Commands;

use Basin\Commands\RecipeBase;
use Basin\RecipeHelper\SMTP;
use Basin\RecipeHelper\MariaDb;

/**
 * Commands for a Wordpress recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    use SMTP;
    use MariaDb;

    public function composer(array $args)
    {
        return $this
            ->taskExec('composer')
            ->args($args)
            ->dir('app/.basin')
            ->run();
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.php-image.name') . ':' .
                        $composer->getLockedVersion($config->get('docker.php-image.composer-name')),
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'fastcgi-wordpress',
                        'VIRTUAL_ROOT' => '/var/www/html',
                    ],
                    'volumes' => [
                        [
                          'type' => 'volume',
                          'source' => 'app',
                          'target' => '/var/www/html',
                        ],
                    ],
                ],
                'wait-for-web' => [
                    'image' => 'dokku/wait:' . $lockedVersion->getVersion('wait'),
                    'restart' => 'no',
                    'command' => '-c web:9000',
                    'depends_on' => [
                        'web' => ['condition' => 'service_started'],
                    ],
                ],
                'wait' => [
                    'image' => 'alpine:3.17',
                    'restart' => 'no',
                    'command' => 'echo all services started',
                    'depends_on' => [
                        'wait-for-web' => ['condition' => 'service_completed_successfully'],
                    ],
                ],
            ],
            'volumes' => [
                'app' => [
                    'external' => false,
                ],
            ],
        ];

        if ($config->get('db.type') === 'mariadb') {
            $dockerCompose = $this->addMariadb($dockerCompose, $config);
        }
        if ($config->get('mail.type') === 'smtp') {
            $dockerCompose = $this->addSMTP($dockerCompose, $config);
        }
        return $this->processDockerCompose($dockerCompose, $config);
    }
}

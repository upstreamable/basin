---
sidebar_position: 5
---

# Troubleshooting

## Files created by basin can't be edited/deleted

Basin is designed as container and the user that runs the tool
inside that environment copies the user id and the group id from the user
that executes basin on the host machine.
Additionally the images used to run the services also follow that pattern so
all the files within the project can be managed by the user from outside the
container.

The user that runs basin doesn't need to be root and sudo is also not needed.
Report a bug if you see a different behavior.

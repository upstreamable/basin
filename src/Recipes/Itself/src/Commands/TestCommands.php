<?php

namespace Basin\Recipes\Itself\Commands;

use Robo\Tasks;

/**
 * Helper commands for testing.
 *
 * @see http://robo.li/
 */
class TestCommands extends Tasks
{
    /**
     * Run phpunit test.
     *
     * @command test:phpunit
     */
    public function phpunit()
    {
        return $this
            ->taskPHPUnit()
            ->files('app/src/tests')
            ->run();
    }
}

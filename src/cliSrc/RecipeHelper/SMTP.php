<?php

namespace Basin\RecipeHelper;

trait SMTP
{
    protected function addSMTP(array $dockerCompose, $config)
    {
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $consoleIO = $this->getContainer()->get('consoleio');

        $activeEnvironment = $config->get('environment.active');
        $smtpEnabled = $config->get('environment.' . $activeEnvironment . '.mail.smtp.enabled');
        $mailpitEnabled = $config->get('environment.' . $activeEnvironment . '.mail.mailpit.enabled');

        $smtpServer = $config->get('mail.smtpServer');
        $smtpUsername = $config->get('mail.smtpUsername');
        $smtpPassword = $config->get('mail.smtpPassword');
        $smtpSecure = $config->get('mail.smtpSecure');
        $smtpAuthtype = $config->get('mail.smtpAuthtype');
        $smtpGlobalSenderIsUsername = $config->get('mail.smtpGlobalSenderIsUsername');
        $smtpSetFromHeader = $config->get('mail.smtpSetFromHeader');
        $mailDomain = $config->get('mail.domain');
        $sendmailPath = $config->get('mail.sendmailPath');

        if ($smtpServer && str_contains($smtpServer, ':')) {
            list($smtpHost, $smtpPort) = explode(':', $smtpServer);
        } else {
            $smtpHost = $smtpServer;
            $smtpPort = 465;
        }

        if (empty($smtpUsername) || empty($smtpPassword) || empty($smtpServer)) {
            $consoleIO->yell('
                In order to send emails some configuration is needed:
                mail.smtpUsername, mail.smtpPassword and mail.smtpServer
                in the .basin/secrets.yml file.
                If mail sending is not needed you can set mail.type = none
                in the .basin/config.yml file
            ', color: 'red');
        }

        if ($smtpEnabled) {
            $dockerCompose['services']['web']['environment']['SMTP_SERVER'] = $smtpServer;
            $dockerCompose['services']['web']['environment']['SMTP_USERNAME'] = $smtpUsername;
            $dockerCompose['services']['web']['environment']['SMTP_NAME'] = $smtpUsername;
            $dockerCompose['services']['web']['environment']['SMTP_PASSWORD'] = $smtpPassword;
            $dockerCompose['services']['web']['environment']['SMTP_HOST'] = $smtpHost;
            $dockerCompose['services']['web']['environment']['SMTP_PORT'] = $smtpPort;
            // Values: ssl,tls or empty.
            $dockerCompose['services']['web']['environment']['SMTP_SECURE'] = $smtpSecure;
            $dockerCompose['services']['web']['environment']['SMTP_TLS'] = $smtpSecure === 'ssl' ? 'on' : 'off';
            $dockerCompose['services']['web']['environment']['SMTP_TLS_STARTTLS'] = $smtpSecure === 'tls' ?
                'on' :
                'off';
            $dockerCompose['services']['web']['environment']['SMTP_AUTHTYPE'] = $smtpAuthtype;
            $dockerCompose['services']['web']['environment']['SMTP_AUTHTYPE_LOWERCASE'] = $smtpAuthtype ?
                strtolower($smtpAuthtype) : $smtpAuthtype;
            $dockerCompose['services']['web']['environment']['SMTP_SET_FROM_HEADER'] = $smtpSetFromHeader;
            $dockerCompose['services']['web']['environment']['PHP_SENDMAIL_PATH'] = $sendmailPath;
        }

        if ($mailDomain) {
            $dockerCompose['services']['web']['environment']['MAIL_DOMAIN'] = $mailDomain;
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MAIL_DOMAIN';
        }

        if ($mailpitEnabled) {
            $dockerCompose['services']['mailpit'] = [
                'image' => 'axllent/mailpit:' . $lockedVersion->getVersion('mailpit'),
                'environment' => [
                    'VIRTUAL_HOST' => 'mailpit.' . $virtualHost->getVirtualHost(),
                    'VIRTUAL_PORT' => '8025',
                ],
            ];
            $dockerCompose['services']['web']['environment']['SMTP_USERNAME'] = '';
            $dockerCompose['services']['web']['environment']['SMTP_NAME'] = '';
            $dockerCompose['services']['web']['environment']['SMTP_PASSWORD'] = '';
            $dockerCompose['services']['web']['environment']['SMTP_SECURE'] = '';
            $dockerCompose['services']['web']['environment']['SMTP_SERVER'] = 'mailpit:1025';
            $dockerCompose['services']['web']['environment']['SMTP_HOST'] = 'mailpit';
            $dockerCompose['services']['web']['environment']['SMTP_PORT'] = '1025';
            $dockerCompose['services']['web']['environment']['PHP_SENDMAIL_PATH'] = $sendmailPath;
            $dockerCompose['services']['web']['environment']['SMTP_AUTHTYPE'] = 'OFF';
            $dockerCompose['services']['web']['environment']['SMTP_AUTHTYPE_LOWERCASE'] = 'off';
        }
        if ($smtpGlobalSenderIsUsername) {
            $dockerCompose['services']['web']['environment']['SMTP_GLOBAL_SENDER_IS_USERNAME']
                = $smtpGlobalSenderIsUsername;
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_GLOBAL_SENDER_IS_USERNAME';
            $dockerCompose['services']['web']['environment']['MAIL_FROM_ADDRESS'] = $smtpUsername;
        }

        if (isset($dockerCompose['services']['web']['environment']['SMTP_SERVER'])) {
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_USERNAME';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_NAME';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_PASSWORD';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_SERVER';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'SMTP_HOST';
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MAIL_FROM_ADDRESS';
        }

        foreach ($config->get('mail.environmentVariablesAliases') ?? [] as $source => $alias) {
            if (!array_key_exists($source, $dockerCompose['services']['web']['environment'])) {
                continue;
            }
            $dockerCompose['services']['web']['environment'][$alias] =
                $dockerCompose['services']['web']['environment'][$source];
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = $alias;
        }

        return $dockerCompose;
    }
}

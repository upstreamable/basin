<?php

namespace Basin\Recipes\WordpressVolume;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('db.type', 'mariadb');
        $config->setDefault('docker.php-image.name', 'upstreamable/wordpress');
        $config->setDefault('docker.php-image.composer-name', 'upstreamable/docker-wordpress');
        $config->setDefault('docker.db_image', 'wodby/mariadb');
        $config->setDefault('docker.defaultContainer.user', 'www-data');
        $config->setDefault('docker.defaultContainer.name', 'web');

        $config->setDefault('passthroughCommands', [
            'composer',
        ]);
        $config->setDefault('composer.root', '.basin/');

        $config->setDefault('db.environmentVariablesAliases', [
            'MYSQL_HOST' => 'WORDPRESS_DB_HOST',
            'MYSQL_USER' => 'WORDPRESS_DB_USER',
            'MYSQL_PASSWORD' => 'WORDPRESS_DB_PASSWORD',
            'MYSQL_DATABASE' => 'WORDPRESS_DB_NAME',
        ]);
        return $config;
    }
}

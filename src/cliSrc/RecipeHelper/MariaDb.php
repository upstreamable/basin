<?php

namespace Basin\RecipeHelper;

trait MariaDb
{
    protected function addMariadb(array $dockerCompose, $config)
    {
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dbPassword = $config->get('db.password') ?? $config->get('cleanProjectName');
        $dockerCompose['services']['db'] = [
            'image' => $config->get('docker.db_image') . ':' . $lockedVersion->getVersion('mariadb'),
            'environment' => [
                'MYSQL_ROOT_PASSWORD' => $dbPassword,
                'MYSQL_PASSWORD' => $dbPassword,
                'MYSQL_DATABASE' => 'db',
                'MYSQL_USER' => 'db',
                // Recommended by Drupal.
                'MYSQL_TRANSACTION_ISOLATION' => 'READ-COMMITTED',
            ],
            'volumes' => [
                [
                    'type' => 'volume',
                    'source' => 'db',
                    'target' => '/var/lib/mysql',
                ],
            ],
        ];
        $dockerCompose['services']['db-ready'] = [
            'image' => 'upstreamable/wait-for-mariadb:' . $lockedVersion->getVersion('wait-for-mariadb'),
            'restart' => 'no',
            'environment' => [
                'MYSQL_ROOT_PASSWORD' => $dbPassword,
                'MYSQL_PASSWORD' => $dbPassword,
                'MYSQL_DATABASE' => 'db',
                'MYSQL_USER' => 'db',
            ],
        ];

        $dockerCompose['services']['web']['depends_on']['db-ready'] = ['condition' => 'service_completed_successfully'];
        $dockerCompose['services']['web']['environment']['DB_CONNECTION'] = 'mysql';
        $dockerCompose['services']['web']['environment']['MYSQL_DATABASE'] = 'db';
        $dockerCompose['services']['web']['environment']['MYSQL_USER'] = 'db';
        $dockerCompose['services']['web']['environment']['MYSQL_PASSWORD'] = $dbPassword;
        $dockerCompose['services']['web']['environment']['MYSQL_HOST'] = 'db';
        $dockerCompose['services']['web']['environment']['MYSQL_PORT'] = '3306';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'DB_CONNECTION';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MYSQL_DATABASE';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MYSQL_USER';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MYSQL_PASSWORD';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MYSQL_HOST';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'MYSQL_PORT';

        foreach ($config->get('db.environmentVariablesAliases') ?? [] as $source => $alias) {
            if (!array_key_exists($source, $dockerCompose['services']['web']['environment'])) {
                continue;
            }
            $dockerCompose['services']['web']['environment'][$alias] =
                $dockerCompose['services']['web']['environment'][$source];
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = $alias;
        }

        $dockerCompose['volumes']['db'] = [
            'external' => false,
        ];

        if ($config->get('db.phpMyAdmin.enable')) {
            $virtualHost = $this->getContainer()->get('virtualhost');
            $version = $lockedVersion->getVersion('phpmyadmin');

            // Only first three version digits are relevant.
            $version = explode('.', $version);
            $version = implode('.', array_slice($version, offset: 0, length: 3));

            $dockerCompose['services']['phpmyadmin'] = [
                'image' => 'phpmyadmin:' . $version,
                'environment' => [
                    'VIRTUAL_HOST' => 'phpmyadmin.' . $virtualHost->getVirtualHost(),
                    'VIRTUAL_PORT' => '80',
                    'PMA_USER' => 'db',
                    'PMA_PASSWORD' => $dbPassword,
                    'PMA_HOST' => 'db',
                ],
            ];
        }
        return $dockerCompose;
    }
}

---
sidebar_position: 1
---

# What is Basin?

A local development environment system and deploy tool based on docker and
distributed as a docker image.

![Basin logo](/img/logo.png)

## Features

* Run local environments as close as possible to production.
* Use the same tools and services for different projects.
* Configure every project in its own code repository.
* Have a web proxy to run many projects at once.
* Standardize tooling for a team.
* Abstract from the complexity of other tools like docker-compose.

## Why so much docker?

There is many advantages to rely on docker.

* The only dependency to run is docker so setup and installation is a breeze.
* Cleaner setup for you environment as the images and container can be removed.
* No other tools or frameworks are needed to run an app like databases or webservers.
* Better portability and reusability.
* No reconfiguring on different hosts.
* Easier updates.

<?php

namespace Basin\Service;

use Consolidation\Config\ConfigInterface;

/**
 * Helper methods related to docker-compose.
 */
class DockerCompose
{
    public function __construct(
        protected ConfigInterface $config,
    ) {
    }

    public function getProjectName()
    {
        $cleanProjectName = $this->config->get('cleanProjectName');
        if (file_exists('app/REVISION')) {
            $cleanProjectName = $cleanProjectName . '-' . $this->sanitize(file_get_contents('app/REVISION'));
        }
        return $cleanProjectName;
    }

    /**
     * Sanitize the name the same way docker-compose does.
     */
    public function sanitize($name)
    {
        return preg_replace('/[^a-z0-9]/', '', strtolower($name));
    }
}

<?php

namespace Basin\Recipes\Drupal;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('db.type', 'mariadb');
        $config->setDefault('cache.type', 'redis');
        $config->setDefault('docker.php-image.name', 'upstreamable/drupal-php');
        $config->setDefault('docker.php-image.composer-name', 'upstreamable/docker-drupal-php');
        $config->setDefault('docker.db_image', 'wodby/mariadb');
        $config->setDefault('docker.redis_image', 'wodby/redis');
        $config->setDefault('docker.volumes.public-files.relative-path', 'files');
        $config->setDefault('docker.defaultContainer.user', 'www-data');
        $config->setDefault('docker.defaultContainer.name', 'web');
        $config->setDefault('mail.type', 'smtp');
        // Override the global site email with the smtp username.
        $config->setDefault('mail.smtpGlobalSenderIsUsername', 'true');
        $config->setDefault('mail.sendmailPath', '/usr/bin/msmtp -t');

        $config->setDefault('mail.environmentVariablesAliases', [
            'SMTP_USERNAME' => 'MSMTP_USER',
            'SMTP_PASSWORD' => 'MSMTP_PASSWORD',
            'SMTP_TLS' => 'MSMTP_TLS',
            'SMTP_TLS_STARTTLS' => 'MSMTP_TLS_STARTTLS',
            'SMTP_HOST' => 'MSMTP_HOST',
            'SMTP_PORT' => 'MSMTP_PORT',
            'SMTP_AUTHTYPE_LOWERCASE' => 'MSMTP_AUTH',
            'MAIL_FROM_ADDRESS' => 'MSMTP_FROM',
            'SMTP_SET_FROM_HEADER' => 'MSMTP_SET_FROM_HEADER',
        ]);

        $config->setDefault('environment.development.php.assertActive', 'On');
        $config->setDefault('environment.production.php.assertActive', 'Off');

        // All deployed environments should automatically update composer and database.
        // The drush deploy is not included for development environments to avoid losing work
        // when starting the project.
        $defaultInit = 'su-exec www-data composer install --no-interaction --no-dev --optimize-autoloader &&
            su-exec www-data drush deploy';
        $config->setDefault('environment.localhost.init-command', $defaultInit);
        $config->setDefault('environment.production.init-command', $defaultInit);
        $defaultInit = 'su-exec www-data composer install --no-interaction';
        $config->setDefault('environment.development.init-command', $defaultInit);

        $config->setDefault('environment.production.cron.job', 'drush cron');
        // Avoid all crons running in the same minute.
        $config->setDefault('environment.production.cron.minute', rand(0, 59));
        // Run during the morning hours.
        $config->setDefault('environment.production.cron.hour', rand(1, 5));
        $config->setDefault('environment.production.cron.day', '*');
        $config->setDefault('environment.production.cron.month', '*');
        $config->setDefault('environment.production.cron.weekday', '*');

        $config->setDefault('environment.development.proxy.fastcgiSendTimeout', '1200s');
        $config->setDefault('environment.development.proxy.fastcgiReadTimeout', '1200s');

        $config->setDefault('passthroughCommands', [
            'composer',
            'drush',
        ]);
        $config->setDefault('composer.root', '');
        return $config;
    }
}

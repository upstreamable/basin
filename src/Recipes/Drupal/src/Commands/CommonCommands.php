<?php

namespace Basin\Recipes\Drupal\Commands;

use Basin\Commands\RecipeBase;
use Basin\RecipeHelper\Mailgun;
use Basin\RecipeHelper\MariaDb;
use Basin\RecipeHelper\Redis;
use Basin\RecipeHelper\SMTP;

/**
 * Commands for a Drupal recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    use SMTP;
    use Mailgun;
    use MariaDb;
    use Redis;

    public function drush(array $args)
    {
        // Use the full path to drush to avoid the drush-launcher (throws warning on php 8.2).
        $this->runPassThroughCommand('/var/www/html/vendor/bin/drush', $args);
    }

    public function composer(array $args)
    {
        $this->runPassThroughCommand('composer', $args);
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $publicFilesPath = $config->get('docker.volumes.public-files.relative-path');
        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');
        $environmentType = $config->get('environment.' . $activeEnvironment . '.type');
        $phpAssertActive = $config->get('environment.' . $activeEnvironment . '.php.assertActive');

        $updateNotificationsEmails = $config->get('update.mailNotifications');

        // When the project has configuration set already install using those file,
        // but when there is no config, for example when initializating a project,
        // perform a normal site install.
        $OnEmptyDatabaseSiteInstallMode = is_dir('app/config') && file_exists('app/config/core.extension.yml') ?
            '--existing-config' :
            '--site-name="' . $config->get('projectName') . '" core/recipes/standard';

        $imageVersion = $composer->getLockedVersion($config->get('docker.php-image.composer-name'));

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.php-image.name') . ':' . $imageVersion,
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'fastcgi-drupal',
                        'VIRTUAL_ROOT' => '/var/www/html/web',
                        'HOST_UID' => getenv('HOST_UID'),
                        'HOST_GID' => getenv('HOST_GID'),
                        'ENVIRONMENT_TYPE' => $environmentType,
                        'PHP_ASSERT_ACTIVE' => $phpAssertActive,
                        'DRUPAL_HASH_SALT' => $config->get('drupal.hash_salt') ?? $config->get('cleanProjectName'),
                        'DRUPAL_PUBLIC_PATH' => $publicFilesPath,
                        'DRUPAL_FIELD_UI_PREFIX_EMPTY' => 'TRUE',
                        'DRUPAL_UPDATE_NOTIFICATION_EMAILS' => $updateNotificationsEmails,
                        'DRUPAL_COMMAND_ON_EMPTY_DB' =>
                            'su-exec www-data composer install --no-interaction && ' .
                            'su-exec www-data drush -y site:install ' . $OnEmptyDatabaseSiteInstallMode,
                        'DRUSH_OPTIONS_URI' => $protocol . '://' . $virtualHost->getVirtualHost(),
                        'SWAP_WWWDATA_UIDGID_FOR_HOST' => 'true',
                        'PHP_FPM_ENV_VARS' => [
                            'DRUPAL_HASH_SALT',
                            'DRUPAL_PUBLIC_PATH',
                            'DRUPAL_FIELD_UI_PREFIX_EMPTY',
                            'DRUPAL_UPDATE_NOTIFICATION_EMAILS',
                            'DRUSH_OPTIONS_URI',
                        ],
                    ],
                    'volumes' => [
                        [
                            'type' => 'bind',
                            'source' => getenv('HOST_WORKING_DIR'),
                            'target' => '/var/www/html',
                        ],
                        [
                          'type' => 'volume',
                          'source' => 'public-files',
                          'target' => '/var/www/html/web/' . $publicFilesPath,
                        ],
                        [
                            'type' => 'volume',
                            'source' => 'private',
                            // Default location in wodby images.
                            'target' => '/mnt/files/private',
                        ],
                    ],
                    'labels' => [
                        getenv('LABEL_REVERSE_DNS') . '.subvolume-paths' => $publicFilesPath,
                    ],
                    // Allow for containers on a linux engine to connect to the host.
                    'extra_hosts' => [
                        'host.docker.internal:host-gateway',
                    ],
                ],
                'wait-for-web' => [
                    'image' => 'dokku/wait:' . $lockedVersion->getVersion('wait'),
                    'restart' => 'no',
                    // Give the web container some time to install composer dependencies, drupal, etc.
                    'command' => '-c web:9000 -t 240',
                    'depends_on' => [
                        'web' => ['condition' => 'service_started'],
                    ],
                ],
                'wait' => [
                    'image' => 'alpine:3.20',
                    'restart' => 'no',
                    'command' => 'echo all services started',
                    'depends_on' => [
                        'wait-for-web' => ['condition' => 'service_completed_successfully'],
                    ],
                ],
            ],
            'volumes' => [
                'public-files' => [
                    'external' => false,
                ],
                'private' => [
                    'external' => false,
                ],
            ],
        ];
        if ($config->get('db.type') === 'mariadb') {
            $dockerCompose = $this->addMariadb($dockerCompose, $config);
        }
        if ($config->get('cache.type') === 'redis') {
            $dockerCompose = $this->addRedis($dockerCompose, $config);
        }
        if ($config->get('mail.type') === 'smtp') {
            $dockerCompose = $this->addSMTP($dockerCompose, $config);
        }
        if ($config->get('environment.' . $activeEnvironment . '.mail.mailgun.enabled')) {
            $dockerCompose = $this->addMailgun($dockerCompose, $config);
        }
        if ($initCommand = $config->get('environment.' . $activeEnvironment . '.init-command')) {
            $dockerCompose = $this->addInitCommand($dockerCompose, $initCommand);
        }
        if ($this->isXdebugEnabled($config, $activeEnvironment)) {
            $dockerCompose = $this->addXDebug(
                $dockerCompose,
                $config->get('environment.' . $activeEnvironment . '.xdebug')
            );
        }
        return $this->processDockerCompose($dockerCompose, $config);
    }

    protected function addInitCommand(array $dockerCompose, $initCommand)
    {
        $dockerCompose['services']['web']['environment']['DRUPAL_INIT_COMMAND'] = $initCommand;
        return $dockerCompose;
    }

    protected function addXDebug(array $dockerCompose, $xDebug)
    {
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG'] = '1';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_CLIENT_HOST'] = $xDebug['client_host'] ??
            'host.docker.internal';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_LOG'] = '/tmp/xdebug.log';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_MODE'] = $xDebug['mode'];
        // By default xdebug is a disabled extension.
        // See: https://wodby.com/docs/1.0/stacks/php/local/#xdebug
        $dockerCompose['services']['web']['environment']['PHP_EXTENSIONS_DISABLE'] = '';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_REMOTE_HOST';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_LOG';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_MODE';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_EXTENSIONS_DISABLE';

        return $dockerCompose;
    }

    protected function isXdebugEnabled($config, $activeEnvironment)
    {
        return is_array($config->get('environment.' . $activeEnvironment . '.xdebug'));
    }

    public function nvim(array $args)
    {
        $hostWorkingDir = getenv('HOST_WORKING_DIR');
        $postRun = $this->getContainer()->get('postrun');
        $config = $this->getContainer()->get('config');

        $dockerComposeProjectName = $this->getContainer()->get('dockercompose')->getProjectName();
        $webDockerCompose = $this->getDockerCompose();

        $args = array_map([$this, 'stringWrap'], $args);

        $postRun->addCommand('mkdir -p ' . $hostWorkingDir . '/.basin/cache/phpactor');
        $postRun->addCommand('mkdir -p ' . $hostWorkingDir . '/.basin/cache/nvim/shada');
        $command = 'docker run --rm --interactive --tty ' .
            '--name=neovim ' .
            '--volume ' . $hostWorkingDir . ':/var/www/html ' .
            '--volume ' . $hostWorkingDir . '/.basin/cache/phpactor:/home/wodby/.cache/phpactor ' .
            '--volume ' . $hostWorkingDir . '/.basin/cache/nvim/shada:/home/wodby/.local/state/nvim/shada ' .
            '--network=' . $dockerComposeProjectName . '_default ';
        foreach ($webDockerCompose['services']['web']['environment'] as $varName => $varValue) {
            if ($varName === 'PHP_FPM_ENV_VARS') {
                continue;
            }
            $command .= '-e ' . $varName . '="' . $varValue . '" ';
        }
            // TODO: Use an updatable version.
        $command .= 'upstreamable/drupal-neovim:0.0.1 ' .
            'nvim ' .
            implode(' ', $args);
        $postRun->addCommand($command);
    }
}

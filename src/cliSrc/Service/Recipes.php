<?php

namespace Basin\Service;

use Symfony\Component\Finder\Finder;

/**
 * Recipe-related methods.
 */
class Recipes
{
    public function __construct(
        public string $currentRecipe,
    ) {
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $finder = new Finder();
        $finder
            ->in('Recipes')
            ->exclude('Itself')
            ->exclude('NoRecipe')
            ->files()
            ->name('info.yml')
            ->followLinks();
        $recipes = [];
        foreach ($finder as $infoYml) {
            $info = yaml_parse($infoYml->getContents());
            $recipes[$infoYml->getPathInfo()->getFilename()] = $info;
        }

        return $recipes;
    }

    /**
     * @return array
     */
    public function getDefinition($recipe = null): array
    {
        if (!$recipe) {
            $recipe = $this->currentRecipe;
        }
        return yaml_parse_file('Recipes/' . $recipe . '/info.yml');
    }
}

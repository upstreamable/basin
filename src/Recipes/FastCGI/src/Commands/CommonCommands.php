<?php

namespace Basin\Recipes\FastCGI\Commands;

use Basin\Commands\RecipeBase;

/**
 * Commands for a FastCGI recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    public function composer(array $args)
    {
        return $this
            ->taskExec('composer')
            ->args($args)
            ->dir('app/.basin')
            ->run();
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.defaultContainer.imageName') . ':' .
                        $composer->getLockedVersion($config->get('docker.defaultContainer.imageNameComposer')),
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'fastcgi',
                        'VIRTUAL_ROOT' => '/var/www/html',
                    ],
                ],
                'wait-for-web' => [
                    'image' => 'dokku/wait:' . $lockedVersion->getVersion('wait'),
                    'restart' => 'no',
                    'command' => '-c web:9000',
                    'depends_on' => [
                        'web' => ['condition' => 'service_started'],
                    ],
                ],
                'wait' => [
                    'image' => 'alpine:3.17',
                    'restart' => 'no',
                    'command' => 'echo all services started',
                    'depends_on' => [
                        'wait-for-web' => ['condition' => 'service_completed_successfully'],
                    ],
                ],
            ],
        ];

        return $this->processDockerCompose($dockerCompose, $config);
    }
}

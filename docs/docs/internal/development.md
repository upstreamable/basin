---
sidebar_position: 1
---

# Internal development

Since Basin is delivered as a docker image modifying its internals involves
some kind of "compiling" process.

In order to facilitate the task of maintaining basin a recipe name `Itself`
exists inside Basin with commands for the most common tasks.

# Setup

Clone the Basin git repository at https://gitlab.com/upstreamable/basin

To ensure the launcher script runs the version for the docker
image `upstreamable/basin:latest` and the `Itself` recipe is used
the repository already contains a `.basin` folder configured.

# Handy commands

```
basin docker:build --dev
```
Builds the image with the `latest-dev` tag

```
docker run -it --rm -v $PWD:/home/basin/app --entrypoint=sh upstreamable/basin:latest-dev
```
Start an interactive CLI session inside the basin image mounting the current
directory

```
docker run -it --rm -v $PWD:/home/basin/app -v
/var/run/docker.sock:/var/run/docker.sock -e "VERSION=latest-dev" -e
HOST_HOME_DIR=$HOME -e
HOST_GLOBAL_CONFIG_DIR=$HOME/.config/basin -e HOST_UID=1000 -e HOST_GID=1000
--entrypoint=sh upstreamable/basin:latest-dev
```

Same as the above command but closest to how it is invoked by the wrapper
script.

# Documentation changes

To see the documentation in a browser the [`docusaurus`](https://docusaurus.io/)
commands need to be used.
First run a nodejs container
```
docker run -it --rm  -v "$PWD":/usr/src/app -p 3000:3000 -w /usr/src/app node:18 bash
```
and then inside the nodejs container run the dev mode for the docusaurus site:
```
cd docs
npm run start -- --host 0.0.0.0
```
The `0.0.0.0` instructs nodejs to expose the site in all the networks
interfaces, including the one that connects the container with the host. Without
this the site is only exposed in the localhost context of the container.

Then http://localhost:3000/ can be visited to see the documentation.

# Continuous integration

When changes are proposed with a merge request, on commits to the main branch
or a tagged release the `.gitlab-ci.yml` references the commands that run.

In order to push updated images into the docker registry credentials are needed
so the variables `DOCKERHUB_USER` and `DOCKERHUB_PASS` are provided as secret
environment settings for the gitlab CI pipeline.

Some steps of the pipeline are executed using the basin image itself, in this
case the latest tagged release is used so in order to make changes regarding
steps that use the basin image they need to be included in a stable release
before changing the `gitlab-ci.yml` that will use them.

# Related repositories

* Proxy https://github.com/upstreamable/nginx-proxy

<?php

namespace Basin\Recipes\Itself\Commands;

use Robo\Tasks;

/**
 * Helper commands to generate packages.
 *
 * @see http://robo.li/
 */
class PackagingCommands extends Tasks
{
    protected const NFPM_CONFIG_PATH = '/tmp/nfpm.yml';
    protected const NFPM_BINARY_PATH = 'app/src/vendor/bin/nfpm';
    protected const SUPPORTED_PACKAGE_TYPES = ['deb', 'rpm', 'apk'];
    protected const BUILD_DIR = 'app/docs/static';

    /**
     * Generate release packages for all supported formats.
     *
     * @command package:generate-all
     */
    public function generateAllPackages($version)
    {
        foreach (self::SUPPORTED_PACKAGE_TYPES as $packageType) {
            $this->generatePackage($version, $packageType);
        }
    }

    /**
     * Generate a package for release.
     *
     * @command package:generate
     */
    public function generatePackage($version, $type = 'deb')
    {
        $collection = $this->collectionBuilder();
        $this->ensureDirectory($collection, self::BUILD_DIR . '/bin/' . $version);
        $collection->addTask(
            $this->taskFilesystemStack()->copy(
                'app/bin/basin',
                self::BUILD_DIR . '/bin/' . $version . '/basin'
            )
        );
        $this->setWrapperScriptVersion($collection, $version);

        $this->ensureDirectory($collection, dirname(self::NFPM_BINARY_PATH));
        $this->ensureNfpmBinaryAvailable($collection);
        $this->generateNfpmConfig($collection, $version);

        $collection->addTask(
            $this->taskExec(self::NFPM_BINARY_PATH)
                 ->arg('pkg')
                 ->option('config', self::NFPM_CONFIG_PATH)
                 ->option('packager', $type)
                 ->option('target', self::BUILD_DIR)
        );

        return $collection->run();
    }

    protected function setWrapperScriptVersion($collection, $version)
    {
        if ($version === 'latest') {
            return;
        }
        $collection->addTask(
            $this->taskReplaceInFile(self::BUILD_DIR . '/bin/' . $version . '/basin')
                 ->regex('/^WRAPPER_SCRIPT_VERSION=.+$/m')
                 ->to('WRAPPER_SCRIPT_VERSION="' . $version . '"')
        );
    }

    private function getCurrentArchitecture()
    {
        return php_uname('m');
    }

    private function getNfpmTarReleaseUrl($versionInfo)
    {
        return str_replace(
            [
                '[VERSION]',
                '[ARCH]',
            ],
            [
                $versionInfo['version'],
                $this->getCurrentArchitecture(),
            ],
            $versionInfo['source'] . $versionInfo['release-path']
        );
    }

    private function ensureDirectory($collection, $directory)
    {
        if (!file_exists($directory)) {
            $collection->taskFilesystemStack()
                ->mkdir($directory);
        }
    }

    private function ensureNfpmBinaryAvailable($collection)
    {
        if (!file_exists(self::NFPM_BINARY_PATH)) {
            $archiveDestination = '/tmp/nfpm.tar.gz';
            $extractDestination = '/tmp/extract/nfpm';
            $versionInfo = yaml_parse_file('app/src/locked-versions.yml')['nfpm'];
            $collection->taskFilesystemStack()->copy(
                $this->getNfpmTarReleaseUrl($versionInfo),
                $archiveDestination
            );
            $collection->taskExtract($archiveDestination)
                       ->to($extractDestination);
            $collection->taskFilesystemStack()
                       ->copy($extractDestination . '/nfpm', self::NFPM_BINARY_PATH);
        }
    }

    private function generateNfpmConfig($collection, $version)
    {
        $composerJson = json_decode(file_get_contents('app/src/composer.json'));
        $maintainer = reset($composerJson->authors);
        [$vendor, $cliName] = explode('/', $composerJson->name);

        // Check https://nfpm.goreleaser.com/configuration for detailed usage.
        $nfpmConfig = [
            'name' => 'basin',
            'vendor' => $vendor,
            'version' => $version,
            'arch' => 'all',
            'platform' => 'linux',
            'section' => 'default',
            'priority' => 'optional',
            'maintainer' => $maintainer->name . ' <' . $maintainer->email . '>',
            'description' => $composerJson->description,
            'homepage' => $composerJson->homepage,
            'recommends' => ['docker-ce'],
            'contents' => [
                [
                  'src' => self::BUILD_DIR . '/bin/' . $version . '/*',
                  'dst' => '/usr/local/bin/',
                ]
            ],
        ];

        $collection->taskWriteToFile(self::NFPM_CONFIG_PATH)
            ->text(yaml_emit($nfpmConfig));
    }
}

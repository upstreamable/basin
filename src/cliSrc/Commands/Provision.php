<?php

namespace Basin\Commands;

use Robo\ResultData;

/**
 * Commands related to provision a server.
 */
trait Provision
{
    /**
     * Install required software in a debian stable server.
     *
     * @command deploy:provision:debian
     */
    public function provisionDebian($environment)
    {
        $collection = $this->collectionBuilder();

        $config = $this->getContainer()->get('config');
        $deployHost = $config->get('environment.' . $environment . '.deploy.host');
        if (!$deployHost) {
            $this->say('No deploy host configured for the "' . $environment . '" environment');
            // TODO. Pass message to the user along with the ResultData.
            return new ResultData(ResultData::EXITCODE_ERROR);
        }

        $ansiblePlaybook = [[
            'name' => 'Provision',
            'hosts' => 'all',
            'vars' => [
              'ansible_become' => 'yes',
              'docker_daemon_config' => $this->getDockerDaemonConfiguration(),
            ],
            'tasks' => [
                [
                    'name' => 'Install apt packages',
                    'apt' => [
                        'name' => [
                            'rsync',
                            'docker.io',
                            // The debian kernel comes with apparmor enabled but no tools to deal with it.
                            // It is not possible to run containers without it
                            // See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=989781
                            'apparmor',
                        ],
                        'update_cache' => 'yes',
                        'state' => 'present',
                    ],
                ],
                [
                    'name' => 'Adding {{ ansible_user }} to the docker group',
                    'user' => [
                        'name' => '{{ ansible_user }}',
                        'groups' => 'docker',
                        'append' => 'yes',
                    ],
                ],
                [
                    'name' => 'Check for existing docker daemon configuration',
                    'stat' => ['path' => '/etc/docker/daemon.json'],
                    'register' => 'docker_daemon_config_stat',
                ],
                [
                    'name' => 'Configure docker to allow more networks',
                    'copy' => [
                        'dest' => '/etc/docker/daemon.json',
                        'content' => '{{ docker_daemon_config | to_nice_json }}',
                    ],
                    'when' => 'not docker_daemon_config_stat.stat.exists',
                ],
                [
                    'name' => 'Restart docker daemon',
                    'ansible.builtin.service' => [
                        'name' => 'docker',
                        'state' => 'restarted',
                    ],
                    'when' => 'not docker_daemon_config_stat.stat.exists',
                ],
            ],
        ]];

        $ansiblePlaybook[0]['vars']['ansible_user'] = $config->get('environment.' . $environment . '.deploy.user') ??
            'basin';

        if ($ansibleUserPassword = $config->get('environment.' . $environment . '.deploy.password')) {
            $ansiblePlaybook[0]['vars']['ansible_password'] = $ansibleUserPassword;
        }
        if ($ansibleBecome = $config->get('environment.' . $environment . '.deploy.become')) {
            $ansiblePlaybook[0]['vars']['ansible_become'] = $ansibleBecome;
        }
        if ($ansibleBecomeMethod = $config->get('environment.' . $environment . '.deploy.become_method')) {
            $ansiblePlaybook[0]['vars']['ansible_become_method'] = $ansibleBecomeMethod;
        }
        if ($ansibleBecomeUser = $config->get('environment.' . $environment . '.deploy.become_user')) {
            $ansiblePlaybook[0]['vars']['ansible_become_user'] = $ansibleBecomeUser;
        }
        if ($ansibleBecomePassword = $config->get('environment.' . $environment . '.deploy.become_password')) {
            $ansiblePlaybook[0]['vars']['ansible_become_password'] = $ansibleBecomePassword;
        }

        $ansiblePlaybookTask = $this
            ->taskExec('ansible-playbook')
            ->setProcessInput(yaml_emit($ansiblePlaybook))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->arg('/dev/stdin');
        if ($deployHost !== 'localhost') {
            // Add a trailing comma to specify the inventory inline instead of a file.
            $ansiblePlaybookTask->option('inventory', $deployHost . ',');
        }

        $collection->addTask($ansiblePlaybookTask);
        $result = $collection->run();
        return $result;
    }

    /**
     * Modified configuration to allow more than 31 networks.
     *
     * @see https://straz.to/2021-09-08-docker-address-pools/
     */
    protected function getDockerDaemonConfiguration()
    {
        return [
            'default-address-pools' => [
                [
                    'base' => '172.17.0.0/12',
                    'size' => 20,
                ],
                [
                    'base' => '192.168.0.0/16',
                    'size' => 24,
                ],
            ],
        ];
    }
}

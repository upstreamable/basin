# Composer notes

Use nyholm/psr7 less than 1.8.0 because php-http/discovery is not compatible yet

php-http/discovery is patched because nyholm/psr7 deprecated its non-psr7
factory and it is still included.

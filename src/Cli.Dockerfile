ARG \
PHP_LOCK_VERSION="8.3.13"
ARG ALPINE_LOCK_VERSION="3.20"
ARG COMPOSER_LOCK_VERSION="2.8.3"
ARG PHPEXTENSIONINSTALLER_LOCK_VERSION="2.7.3"

FROM composer:${COMPOSER_LOCK_VERSION} as composer
FROM mlocati/php-extension-installer:${PHPEXTENSIONINSTALLER_LOCK_VERSION} as php-extension-installer
FROM php:${PHP_LOCK_VERSION}-cli-alpine${ALPINE_LOCK_VERSION}

# Development files will be cleaned at the end for production images.
ARG IMAGE_VARIANT=production

ENV PACKAGE_VENDOR=upstreamable
# A writable dir to place executables.
ENV BASIN_BIN_DIR=/home/basin/vendor/bin
# Variables depending on others need their own ENV statement.
ENV PATH=${BASIN_BIN_DIR}:${PATH} \
  LABEL_REVERSE_DNS="com.${PACKAGE_VENDOR}.basin" \
  GIT_LOCK_VERSION=2.45.2-r0 \
  SUEXEC_LOCK_VERSION=0.2-r3 \
  SHADOW_LOCK_VERSION=4.15.1-r0 \
  PATCH_LOCK_VERSION=2.7.6-r10 \
  LASTVERSION_LOCK_VERSION="3.5.2-r1" \
  DOCKERCOMPOSE_LOCK_VERSION="2.31.0" \
  ANSIBLE_LOCK_VERSION="9.5.1-r0" \
  ANSISTRANO_LOCK_VERSION="3.14.0" \
  RSYNC_LOCK_VERSION="3.3.0-r0" \
  GO_LOCK_VERSION="1.22.9-r0" \
  GOBIN=/usr/local/bin

WORKDIR /home/basin

COPY --from=php-extension-installer /usr/bin/install-php-extensions $BASIN_BIN_DIR/
COPY --from=composer /usr/bin/composer $BASIN_BIN_DIR/
# Include lint configuration and patches.
COPY .composer .composer
COPY \
  composer.json \
  composer.lock \
  locked-versions.yml \
  phpstan.neon \
  .phpcs.xml \
  ./

# Fail on pipes (like the curl command to download docker-compose)
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# When docker/compose updates moby/buildkit to a recent version in their go.mod file maybe
# go get github.com/docker/compose/v2/cmd/compose@v2.0.1
# Can be attempted.
# Ignore hadolint until https://github.com/hadolint/hadolint/issues/366 is solved.
# hadolint ignore=DL3042
RUN \
  # Create a user named after the package.
  adduser --disabled-password basin && \
  apk add --no-cache \
    # Required by many composer operations. \
    git="$GIT_LOCK_VERSION" \
    # Required by the entrypoint.sh script. \
    su-exec="$SUEXEC_LOCK_VERSION" \
    # Contains usermod and groupmod for entrypoint. \
    shadow="$SHADOW_LOCK_VERSION" \
    # Required by composer-patches. \
    patch="$PATCH_LOCK_VERSION" \
    # Ansible connections. \
    openssh-client-default=9.7_p1-r4 \
    sshpass=1.10-r0 \
    lastversion="$LASTVERSION_LOCK_VERSION" \
    ansible="$ANSIBLE_LOCK_VERSION" \
    rsync="$RSYNC_LOCK_VERSION" \
    go="$GO_LOCK_VERSION" \
    && \
  go install github.com/commander-cli/commander/v2/cmd/commander@v2.5.0 && \
  # Decode YAML faster.
  install-php-extensions yaml && \
  ansible-galaxy role install --roles-path /usr/share/ansible/roles ansistrano.deploy,"$ANSISTRANO_LOCK_VERSION" && \
  #apk del alpine-sdk && \
  # @todo php-http/discovery is only patched on the second try.
  if test $IMAGE_VARIANT = "production"; then \
    composer install --no-dev --optimize-autoloader ; \
    composer install --no-dev --optimize-autoloader ; \
  else \
    composer install ; \
    composer install ; \
  fi && \
  composer clear-cache && \
  # Cleanup
  rm /usr/local/bin/phpdbg /usr/local/bin/php-cgi && \
  mkdir \
    # Mount for host global config.
    globalconfig \
    # Create a volume to share files with other containers.
    shared \
    && \
  curl -fSL "https://github.com/docker/compose/releases/download/v${DOCKERCOMPOSE_LOCK_VERSION}/docker-compose-$(uname -s|tr DL dl)-$(uname -m)" --create-dirs -o /usr/local/bin/docker-compose && \
  chmod +x /usr/local/bin/docker-compose

COPY entrypoint.sh Entrypoint.php bin ${BASIN_BIN_DIR}/

RUN \
  if test $IMAGE_VARIANT = "production"; then \
    apk del go && rm -rf tests /home/root/go /usr/local/bin/commander ; \
  fi && \
  chown -R basin:basin /home/basin

# Copy main code at the end so the layers above are not rebuilt when doing common modifications.
# Copy directories.
# They can not be in the same copy instruction as they need the destination directory name.
COPY ansible /etc/ansible
COPY tests tests
COPY cliSrc cliSrc
# New recipes can be addded at runtime.
COPY --chown=basin:basin Recipes Recipes

ENTRYPOINT ["entrypoint.sh"]

LABEL org.opencontainers.image.title=basin \
  org.opencontainers.image.description="Local development environment and deploy tool based on docker" \
  org.opencontainers.image.authors="Rodrigo Aguilera <hi@rodrigoaguilera.net>" \
  org.opencontainers.image.vendor=$PACKAGE_VENDOR \
  org.opencontainers.image.licenses=GPL-3.0-or-later \
  org.opencontainers.image.url=https://basin.upstreamable.com \
  org.opencontainers.image.source=https://gitlab.com/upstreamable/basin/ \
  org.opencontainers.image.documentation=https://basin.upstreamable.com

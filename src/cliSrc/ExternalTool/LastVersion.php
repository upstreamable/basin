<?php

namespace Basin\ExternalTool;

use Consolidation\Config\ConfigInterface;

/**
 * Wrapper for the python tool 'lastversion'.
 */
class LastVersion
{
    public function __construct(
        protected ConfigInterface $config,
    ) {
    }

    /**
     * @return string
     */
    public function getLatestVersion(string $source, string $major = null): string
    {
        $opts = ' ';
        if ($major) {
            $opts .= '--major ' . $major . ' ';
        }

        $command = 'lastversion' . $opts . escapeshellcmd($source);

        if ($token = $this->config->get('github.api_token')) {
            $command = 'GITHUB_API_TOKEN=' . $token . ' ' . $command;
        }

        $output = (string) shell_exec($command);
        return trim($output);
    }
}

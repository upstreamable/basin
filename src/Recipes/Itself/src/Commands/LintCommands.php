<?php

namespace Basin\Recipes\Itself\Commands;

use Robo\Tasks;

/**
 * Helper commands to lint files.
 *
 * @see http://robo.li/
 */
class LintCommands extends Tasks
{
    /**
     * Lint composer files.
     *
     * @command lint:composer
     */
    public function composer()
    {
        return $this
            ->taskComposerValidate()
            ->workingDir('app/src')
            ->run();
    }

    /**
     * Analyze php with phpstan.
     *
     * @command lint:phpstan
     */
    public function phpstan()
    {
        return $this
            ->taskExec('phpstan')
            ->arg('analyze')
            ->option('memory-limit', '1G')
            ->run();
    }
}

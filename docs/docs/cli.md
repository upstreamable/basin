---
sidebar_position: 3
---

# CLI (Command-line interface)

## Global options

The following options are available with every command:

* **--verbose (-v):** Increase verbosity of messages.
* **--help (-h):** Display help information.
* **--root (-r):** If specified, use the given directory as working directory.
* **--ansi:** Force ANSI output.
* **--no-ansi:** Disable ANSI output.
* **--version (-V):** Display this application version.

<?php

namespace Basin\Recipes\Nextcloud;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('db.type', 'mariadb');
        $config->setDefault('cache.type', 'redis');
        $config->setDefault('docker.php-image.name', 'upstreamable/nextcloud');
        $config->setDefault('docker.php-image.composer-name', 'upstreamable/docker-nextcloud');
        $config->setDefault('docker.db_image', 'wodby/mariadb');
        $config->setDefault('docker.redis_image', 'wodby/redis');
        $config->setDefault('docker.defaultContainer.user', 'www-data');
        $config->setDefault('docker.defaultContainer.name', 'web');

        $config->setDefault('mail.type', 'smtp');
        $config->setDefault('mail.smtpSecure', 'ssl');
        $config->setDefault('mail.smtpAuthtype', 'LOGIN');

        $config->setDefault('environment.production.cron.job', 'cron');
        // Avoid all crons running in the same minute.
        $config->setDefault('environment.production.cron.minute', '*/' . rand(10, 15));
        $config->setDefault('environment.production.cron.hour', '*');
        $config->setDefault('environment.production.cron.day', '*');
        $config->setDefault('environment.production.cron.month', '*');
        $config->setDefault('environment.production.cron.weekday', '*');

        $config->setDefault('nextcloud.adminUser', 'admin');

        $config->setDefault('passthroughCommands', [
            'composer',
            'occ',
        ]);
        $config->setDefault('composer.root', '.basin/');
        return $config;
    }
}

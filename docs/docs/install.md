---
sidebar_position: 2
---

# Install

## Requirements

* Docker
* A user with permissions to run docker containers

So far it has only been tested on Linux but being a docker-based tool
support for other operating systems should come soon.

## Install options

The install consists of a wrapper script that runs the main docker
container. This script can be downloaded with different methods:


### DEB (Ubuntu, Debian), RPM(Fedora, CentOS) or APK (Alpine) package

Install the latest package from the
[releases page](https://gitlab.com/upstreamable/basin/-/releases)

### Place the script in the $PATH

Download the latest stable script into a location included in `PATH`
and change permissions to be able to execute it.
```
sudo curl -L -o /usr/local/bin/basin https://gitlab.com/upstreamable/basin/-/releases/permalink/latest/downloads/basin && sudo chmod +x /usr/local/bin/basin
```

### Download from the repo

Get the `basin` script from the
[`/bin` folder](https://gitlab.com/upstreamable/basin/-/tree/0.x/bin) and
be sure you place it somewhere in the `$PATH`.

Note that this install method is not pinned to any release and will use
the docker image tagged `latest` at the time of installation.
It is recommended to run `basin self-update` as a first command to pin
the installation to the latest stable.

## Run the CLI tool

Type `basin` in your terminal. When it is launched for the first time it will
download the docker image with all the code and tools inside.

Running `basin` without arguments should list the commands available.

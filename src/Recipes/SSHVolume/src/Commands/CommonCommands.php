<?php

namespace Basin\Recipes\SSHVolume\Commands;

use Basin\Commands\RecipeBase;

/**
 * Commands for a SSHVolume recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    public function composer(array $args)
    {
        return $this
            ->taskExec('composer')
            ->args($args)
            ->dir('app/.basin')
            ->run();
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.sshd-image.name') . ':' .
                        $composer->getLockedVersion($config->get('docker.sshd-image.composer-name')),
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'static-volume',
                        'SSHD_USER' => $config->get('sshd.user'),
                        'SSHD_PASSWORD' => $config->get('sshd.password'),
                    ],
                    'volumes' => [
                        [
                          'type' => 'volume',
                          'source' => 'app',
                          'target' => '/mnt',
                        ],
                    ],
                    'ports' => [
                        [
                            'target' => 22,
                            'published' => $config->get('sshd.port'),
                            'mode' => 'host'
                        ]
                    ],
                ],
            ],
            'volumes' => [
                'app' => [
                    'external' => false,
                ],
            ],
        ];
        return $this->processDockerCompose($dockerCompose, $config);
    }
}

<?php

namespace Basin\Commands;

use Robo\Tasks;

/**
 * Commands for performing isolated tasks.
 *
 * @see http://robo.li/
 */
class UtilityCommands extends Tasks
{
    /**
     * Search and replace a string in a file.
     *
     * @command util:search-replace
     */
    public function searchAndReplace($search, $replace, $filepath)
    {
        return $this
            ->taskReplaceInFile('app/' . $filepath)
            ->from($search)
            ->to($replace)
            ->run();
    }
}

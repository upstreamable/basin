<?php

namespace Basin;

use Basin\ExternalTool\LastVersion;
use Basin\Hook\PostRun;
use Basin\Service\Composer;
use Basin\Service\Docker;
use Basin\Service\DockerCompose;
use Basin\Service\LockedVersion;
use Basin\Service\Proxy;
use Basin\Service\Recipes;
use Basin\Service\VirtualHost;
use Consolidation\AnnotatedCommand\CommandFileDiscovery;
use Consolidation\Config\Loader\ConfigProcessor;
use Consolidation\Config\Loader\YamlConfigLoader;
use League\Container\Container;
use Robo\Robo;
use Robo\Runner as RoboRunner;
use Robo\Symfony\ConsoleIO;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Filesystem\Filesystem;

class Runner extends RoboRunner
{
    private string $recipe;

    /**
     * {@inheritdoc}
     *
     * @param array<string> $argv
     */
    public function execute($argv, $appName = null, $appVersion = null, $output = null)
    {
        $app = new Application($appName, $appVersion);
        $app->setAutoExit(false);

        // Config files ordered by priority.
        // The files with a higher index will override the ones with a
        // lower index so the global configuration will be overriden
        // by the local configuration.
        $configFiles = [
            'globalconfig/config.yml',
            'globalconfig/secrets.yml',
            'app/.basin/config.yml',
            'app/.basin/secrets.yml',
            'app/.basin/deploy.yml',
        ];

        $config = new \Robo\Config\Config();
        $this->loadConfiguration($configFiles, $config);

        $config->setDefault('recipe', 'NoRecipe');
        $this->recipe = $config->get('recipe');

        if (!is_dir('Recipes/' . $this->recipe)) {
            $filesystem = new Filesystem();
            $filesystem->symlink('../app/.basin/Recipe', 'Recipes/' . $this->recipe);
            if (!is_dir('Recipes/' . $this->recipe)) {
                echo 'Recipe "' . $this->recipe . '" is not recognized';
                return 1;
            }
        }

        $config = $this->setConfigDefaults($config);

        // Configure the container like RoboRunner does on the run() method
        // so services can be added.
        $argvInput = $this->getPreparedInput($argv, $config);

        if (!$output) {
            $output = new ConsoleOutput();
        }
        $container = new Container();

        Robo::configureContainer($container, $app, $config, $argvInput, $output, $this->classLoader);
        $this->addServices($container, $config);

        Robo::finalizeContainer($container);
        $this->setContainer($container);

        // Automatically register a shutdown function and
        // an error handler when we provide the container.
        $this->installRoboHandlers();

        $discovery = new CommandFileDiscovery();

        $paths = ['\\' . __NAMESPACE__ . '\\Commands' => 'cliSrc/Commands'];
        $paths['\\' .  __NAMESPACE__ . '\\Recipes\\' . $this->recipe . '\\Commands'] =
            'Recipes/' . $this->recipe . '/src/Commands';
        $commandClasses = $discovery->discover($paths);

        return $this->run($argvInput, $output, $app, $commandClasses, $this->classLoader);
    }

    /**
     * Loads the provided config files into the runtime configuration.
     */
    protected function loadConfiguration($paths, $config)
    {
        $loader = new YamlConfigLoader();
        $processor = new ConfigProcessor();
        $processor->add($config->export());
        foreach ($paths as $path) {
            $processor->extend($loader->load($path));
        }
        $config->runtimeConfig()->import($processor->export());
    }

    /**
     * Get the input object.
     *
     * @param array $argv
     * @param \Consolidation\Config\ConfigInterface $config;
     */
    protected function getPreparedInput(array $argv, $config)
    {
        $argv = $this->cleanArgv($argv);
        $argvInput = new ArgvInput($argv);
        if (in_array($argvInput->getFirstArgument(), $config->get('passthroughCommands') ?? [], true)) {
            // Assume the passthroug command is in the position 1 of the array.
            array_splice($argv, 2, 0, ['--']);
            $argvInput = new ArgvInput($argv);
        }
        return $argvInput;
    }

    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    protected function setConfigDefaults($config)
    {
        $config->setDefault('projectName', basename(getenv('HOST_WORKING_DIR')));
        $projectName = $config->get('projectName');
        $config->setDefault('cleanProjectName', strtolower(preg_replace('/[^a-zA-Z0-9-]/', '-', $projectName)));

        $config->setDefault('localAddress', '127.0.0.1');
        $config->setDefault('proxy.http_port', '80');
        $config->setDefault('proxy.https_port', '443');

        // TODO: document that "active" is not a valid name for an environment.
        $config->setDefault('environment.active', 'development');
        $config->setDefault('environment.development.type', 'development');
        $config->setDefault('environment.development.http.protocol', 'http');
        $config->setDefault('environment.development.service.restart', 'no');
        $config->setDefault('environment.development.mail.mailpit.enabled', true);
        $config->setDefault('environment.development.virtualhost.suffix', '');
        // TODO: Document other options like sslip.io.
        $config->setDefault('environment.development.dns.service', 'localhost');

        $config->setDefault('environment.production.type', 'production');
        $config->setDefault('environment.production.virtualhost.suffix', '');
        $config->setDefault('environment.production.dns.service', '');
        // When the project is named as the main route there is no need to specify it.
        $config->setDefault('environment.production.routes.main', $projectName);
        $config->setDefault('environment.production.http.protocol', 'https');
        $config->setDefault('environment.production.service.restart', 'always');
        $config->setDefault('environment.production.mail.smtp.enabled', true);
        $config->setDefault('environment.production.deploy.path', '~/.config/basin/deploys');

        $config->setDefault('environment.localhost.type', 'production');
        $config->setDefault('environment.localhost.http.protocol', 'http');
        $config->setDefault('environment.localhost.service.restart', 'no');
        $config->setDefault('environment.localhost.virtualhost.suffix', 'deploy');
        $config->setDefault('environment.localhost.deploy.host', 'localhost');
        $config->setDefault('environment.localhost.dns.service', 'localhost');
        // Internal path for doing deploys inside the config folder.
        $config->setDefault(
            'environment.localhost.deploy.path',
            '/home/basin/globalconfig/deploys'
        );

        // Needed to be able to translate between the container path to the actual
        // host path that needs to be mounted.
        $config->setDefault(
            'environment.localhost.deploy.pathOnDockerHost',
            getenv('HOST_GLOBAL_CONFIG_DIR') . '/deploys'
        );

        $config->setDefault('ansistrano.allow_anonymous_stats', false);

        if ($this->classLoader) {
            $baseNamespace = __NAMESPACE__ . '\\Recipes\\' . $this->recipe . '\\';
            $this->classLoader->addPsr4(
                $baseNamespace,
                'Recipes/' . $this->recipe . '/src'
            );
        }

        $class = '\\' .  __NAMESPACE__ . '\\Recipes\\' . $this->recipe . '\\Config';

        // Having a Config class in the recipe is optional.
        if (class_exists($class)) {
            call_user_func([$class, 'setConfigDefaults'], $config);
        }

        return $config;
    }

    /**
     * Add new services.
     *
     * @param \Psr\Container\ContainerInterface $container
     * @param \Consolidation\Config\ConfigInterface $config
     *
     * @return \Psr\Container\ContainerInterface
     */
    protected function addServices($container, $config)
    {
        if (!$container instanceof Container) {
            return $container;
        }

        $container->addShared('docker', Docker::class);
        $container->addShared('lastversion', LastVersion::class)
            ->addArgument('config');
        $container->addShared('postrun', PostRun::class);
        $container->addShared('composer', Composer::class)
            ->addArgument('config');
        $container->addShared('lockedversion', LockedVersion::class);
        $container
            ->addShared('recipes', Recipes::class)
            ->addArgument($this->recipe);
        $container
            ->addShared('virtualhost', VirtualHost::class)
            ->addArgument('config');
        $container
            ->addShared('proxy', Proxy::class)
            ->addArgument('config')
            ->addArgument('docker')
            ->addArgument('dockercompose')
            ->addArgument('recipes')
            ->addArgument('virtualhost')
            ->addArgument('lockedversion');
        $container
            ->addShared('dockercompose', DockerCompose::class)
            ->addArgument('config');
        $container
            ->addShared('consoleio', ConsoleIO::class)
            ->addArgument('input')
            ->addArgument('output');
        return $container;
    }


    /**
     * Remove arguments that are already processed by the wrapper script.
     *
     * @param array $argv;
     *
     * @return array
     */
    protected function cleanArgv($argv)
    {
        $args = ['-r', '--root'];
        foreach ($args as $arg) {
            $index = array_search($arg, $argv, true);
            if ($index === false) {
                continue;
            }
            unset($argv[$index]);
            // Remove also the value given for the argument.
            unset($argv[$index + 1]);
            // Reindex array.
            $argv = array_values($argv);
        }

        return $argv;
    }
}

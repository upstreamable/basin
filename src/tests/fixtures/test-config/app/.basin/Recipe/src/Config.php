<?php

namespace Basin\Recipes\FakeRecipe;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('db.type', 'mariadb');
        return $config;
    }
}

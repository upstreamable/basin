<?php

namespace Basin\Service;

use Consolidation\Config\ConfigInterface;

/**
 * Modify proxy definition.
 */
class Proxy
{
    protected array $dockerCompose;
    public bool $projectIsStopping = false;

    public function __construct(
        protected ConfigInterface $config,
        protected Docker $docker,
        protected DockerCompose $dockerComposeService,
        protected Recipes $recipes,
        protected VirtualHost $virtualHost,
        protected LockedVersion $lockedVersion
    ) {
    }

    public function getDockerCompose()
    {
        $config = $this->config;

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');
        // TODO: Check for proxy restart already set to always and use that if proxy is running.
        $restartProxy = $config->get('environment.' . $activeEnvironment . '.service.restart');

        $httpPort = $config->get('proxy.http_port');
        $httpsPort = $config->get('proxy.https_port');
        $virtualHost = $this->virtualHost->getVirtualHost();

        $fastcgiSendTimeout = $config->get('environment.' . $activeEnvironment . '.proxy.fastcgiSendTimeout');
        $fastcgiReadTimeout = $config->get('environment.' . $activeEnvironment . '.proxy.fastcgiSendTimeout');

        $this->dockerCompose = [
          'services' => [
              'nginx' => [
                  'image' => 'upstreamable/nginx-proxy:' . $this->lockedVersion->getVersion('proxy'),
                  // Generate hash upstream names to avoid conflict with FQDNs.
                  'environment' => [
                      'SHA1_UPSTREAM_NAME' => 'true',
                      'FASTCGI_SEND_TIMEOUT' => $fastcgiSendTimeout,
                      'FASTCGI_READ_TIMEOUT' => $fastcgiReadTimeout,
                  ],
                  'restart' => $restartProxy,
                  'ports' => [
                      [
                          'target' => 80,
                          'published' => intval($httpPort),
                      ],
                      [
                          'target' => 443,
                          'published' => intval($httpsPort),
                      ],
                  ],
                  'volumes' => [
                      '/var/run/docker.sock:/tmp/docker.sock:ro',
                      // Certs are declared as a volume so a name must be given to avoid anonymous volumes.
                      // Used when issuing certs with the companion.
                      'certs:/etc/nginx/certs',
                      'dhparam:/etc/nginx/dhparam',
                      'shared:/shared',
                  ],
                  // To avoid using a custom image in the future create
                  // the symlink that basin requires on runtime.
                  'entrypoint' => [
                      'bash',
                      '-c',
                      'mkdir -p /shared/proxy-vhost.d && ' .
                      'rm -rf /etc/nginx/vhost.d && ' .
                      'ln -s /shared/proxy-vhost.d /etc/nginx/vhost.d && ' .
                      '/app/docker-entrypoint.sh forego start -r',
                  ],
                  'networks' => ['default'],
              ],
          ],
          'volumes' => [
            'dhparam' => [
                'external' => false,
            ],
            'certs' => [
                'external' => false,
            ],
            'shared' => [
                'external' => true,
                'name' => 'basin-shared',
            ],

          ],
        ];

        // Pending clarify: https://github.com/nginx-proxy/nginx-proxy/issues/1453
        if ($config->get('environment.' . $activeEnvironment . '.ipv6')) {
            $this->dockerCompose['services']['nginx']['environment']['ENABLE_IPV6'] = 'true';
        }

        $this->attachRunningProjectsToProxy();

        if ($this->projectIsStopping) {
            // Remove the current project volumes.
            $this->dockerCompose['services']['nginx']['volumes'] = array_diff(
                $this->dockerCompose['services']['nginx']['volumes'],
                $this->getProjectVolumesForProxy($virtualHost)
            );
            $this->dockerCompose['services']['nginx']['networks'] = array_diff(
                $this->dockerCompose['services']['nginx']['networks'],
                [$this->dockerComposeService->getProjectName()]
            );
            $this->removeRedirects();
        } else {
            // Add the current project volumes.
            $this->dockerCompose['services']['nginx']['volumes'] = array_merge(
                $this->dockerCompose['services']['nginx']['volumes'],
                $this->getProjectVolumesForProxy($virtualHost)
            );
            // Add the current project networks.
            $this->dockerCompose['services']['nginx']['networks'] = array_merge(
                $this->dockerCompose['services']['nginx']['networks'],
                [$this->dockerComposeService->getProjectName()]
            );
            $this->addRedirects();
        }

        // TODO: Mixing apps that add the companion with others that do not
        // can result in the companion being stopped.
        // Always add it when is already running.
        if ($protocol === 'https') {
            $this->addCompanion();
        }
        $this->addOrphanNamedVolumes();
        $this->addNetworkDefinitions();
        $this->processDockerCompose();
        return $this->dockerCompose;
    }

    /**
     * Recreate the mounts on the proxy.
     */
    protected function attachRunningProjectsToProxy()
    {
        $workingDirlabel = getenv('LABEL_REVERSE_DNS') . '.host-working-dir';
        $recipelabel = getenv('LABEL_REVERSE_DNS') . '.recipe';
        $subvolumePathsLabel = getenv('LABEL_REVERSE_DNS') . '.subvolume-paths';
        $dockerComposeProjectLabel = 'com.docker.compose.project';

        foreach ($this->docker->getContainersAttachedToProxyWithAllDetails() as $container) {
            if (!$container->getConfig()->getLabels()->offsetExists($workingDirlabel)) {
                continue;
            }
            $labels = $container->getConfig()->getLabels();
            $virtualHosts = explode(',', $this->docker->getEnvVariableFromContainer($container, 'VIRTUAL_HOST'));
            $mainVirtualHost = reset($virtualHosts);
            $this->dockerCompose['services']['nginx']['volumes'] = array_merge(
                $this->dockerCompose['services']['nginx']['volumes'],
                $this->getProjectVolumesForProxy(
                    $mainVirtualHost,
                    $labels[$dockerComposeProjectLabel],
                    $labels[$recipelabel],
                    $labels[$workingDirlabel],
                    $labels[$subvolumePathsLabel] ?? null,
                )
            );
            $this->dockerCompose['services']['nginx']['networks'] = array_merge(
                $this->dockerCompose['services']['nginx']['networks'],
                [$labels[$dockerComposeProjectLabel]]
            );
        }
    }

    /**
     * Volumes to mount in the proxy to serve assets directly from it.
     *
     * @return array<string>
     */
    protected function getProjectVolumesForProxy(
        $virtualHost,
        $dockerComposeProjectName = null,
        $recipe = null,
        $projectWorkingDir = null,
        $subvolumePaths = null,
    ): array {
        $volumes = [];

        if (!$projectWorkingDir) {
            $projectWorkingDir = getenv('HOST_WORKING_DIR');
        }

        if (!$dockerComposeProjectName) {
            $dockerComposeProjectName = $this->dockerComposeService->getProjectName();
        }

        $recipeDefinition = $this->recipes->getDefinition($recipe);
        if (array_key_exists('proxy-volumes', $recipeDefinition)) {
            $proxyVolumes = $recipeDefinition['proxy-volumes'];
            if (str_starts_with($proxyVolumes['assets-webroot'], '/')) {
                // The webroot is mounted from the host.
                $volumes[] =
                    $projectWorkingDir . $proxyVolumes['assets-webroot'] .
                    ':/mnt/assets/webroot_' . $virtualHost;
            } else {
                // The webroot is a volume.
                $volumes[] =
                    'assets-' . $dockerComposeProjectName . '_' . $proxyVolumes['assets-webroot'] .
                    ':/mnt/assets/webroot_' . $virtualHost;
            }

            if (array_key_exists('assets-subvolumes', $proxyVolumes)) {
                // When the paths come from a running container label they should be separated by commas
                // and in the same order than in the recipe definition.
                if ($subvolumePaths) {
                    $subvolumePaths = explode(',', $subvolumePaths);
                }
                foreach ($proxyVolumes['assets-subvolumes'] as $subvolumeName) {
                    $subvolumeRelativePath = $subvolumePaths ?
                        array_shift($subvolumePaths) :
                        $this->config->get('docker.volumes.' . $subvolumeName . '.relative-path');

                    $volumes[] =
                        'assets-' . $dockerComposeProjectName . '_' . $subvolumeName .
                        ':/mnt/assets/webroot_' . $virtualHost . '/' . $subvolumeRelativePath;
                }
            }
        }
        return $volumes;
    }

    /**
     * Add the acme companion service to the docker compose definition.
     */
    protected function addCompanion()
    {
        $volumesInCommon = [
            'html:/usr/share/nginx/html',
            'certs:/etc/nginx/certs',
        ];
        $this->dockerCompose['services']['companion'] = [
            'image' => 'nginxproxy/acme-companion:' . $this->lockedVersion->getVersion('acme-companion'),
            // Only added for production environments.
            'restart' => 'always',
            'depends_on' => ['nginx'],
            'volumes' => [
                'shared:/shared',
                'acme:/etc/acme.sh',
                '/var/run/docker.sock:/var/run/docker.sock:ro',
            ],
            'networks' => ['default'],
            // Prepend a symlink creation before the normal entrypoint.
            'entrypoint' => [
              '/bin/bash',
              '-c',
              // Setting an entrypoint deletes the default command so
              // the CMD from the image needs to be passed to the entrypoint.
              'if [ ! -d /etc/nginx/vhost.d ]; then ln -sf "/shared/proxy-vhost.d" "/etc/nginx/vhost.d"; fi &&
               /app/entrypoint.sh /bin/bash /app/start.sh',
            ],
        ];
        $this->dockerCompose['services']['companion']['volumes'] = array_merge(
            $volumesInCommon,
            $this->dockerCompose['services']['companion']['volumes']
        );
        $this->dockerCompose['services']['nginx']['volumes'] = array_merge(
            $volumesInCommon,
            $this->dockerCompose['services']['nginx']['volumes']
        );

        // Add the new volumes to the global declaration.
        foreach (['html', 'acme'] as $newVolume) {
            $this->dockerCompose['volumes'][$newVolume] = [
                'external' => false,
            ];
        }

        $this->dockerCompose
            ['services']
            ['nginx']
            ['labels']
            ['com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy'] = '';
    }

    /**
     * Ensure all named volumes have definition in the volumes section.
     *
     * This is needed because volumes are added to the proxy
     * wihtout their global volumes definition.
     */
    protected function addOrphanNamedVolumes()
    {
        foreach ($this->dockerCompose['services']['nginx']['volumes'] as $volume) {
            if (str_starts_with($volume, '/')) {
                continue;
            }
            list($volumeName,) = explode(':', $volume);
            if (array_key_exists($volumeName, $this->dockerCompose['volumes'])) {
                continue;
            }
            if (str_starts_with($volumeName, 'assets-')) {
                $externalVolumeName = substr($volumeName, 7);
                if (!$this->docker->volumeExists($externalVolumeName)) {
                    $this->docker->createDockerComposeVolume($externalVolumeName);
                }
                $this->dockerCompose['volumes'][$volumeName] = [
                    'external' => true,
                    'name' => $externalVolumeName,
                ];
            } else {
                $this->dockerCompose['volumes'][$volumeName] = ['external' => false];
            }
        }
    }

    /**
     * Create network entries for the nginx container.
     */
    protected function addNetworkDefinitions()
    {
        foreach ($this->dockerCompose['services']['nginx']['networks'] as $network) {
            if ($network === 'default') {
                continue;
            }
            $networkName = $network . '_default';
            if (!$this->docker->networkExists($networkName)) {
                $this->docker->createDockerComposeNetwork($networkName);
            }
            $this->dockerCompose['networks'][$network] = [
              'external' => true,
              'name' => $networkName,
            ];
        }
    }

    /**
     * Ensure the volume section has increasing numeric indices.
     *
     * Required by docker-compose.
     */
    protected function processDockerCompose()
    {
        // Transform into a plain array and remove duplicates.
        $this->dockerCompose['services']['nginx']['volumes'] = array_values(array_unique(
            $this->dockerCompose['services']['nginx']['volumes']
        ));
        $this->dockerCompose['services']['nginx']['networks'] = array_values(array_unique(
            $this->dockerCompose['services']['nginx']['networks']
        ));
    }

    /**
     * Remove nginx location files for the redirected domains.
     */
    protected function removeRedirects()
    {
        foreach ($this->getRedirectFiles() as $path) {
            if (file_exists($path)) {
                unlink($path);

                // When the directory is left empty also delete it.
                if (empty(glob(dirname($path) . '/*'))) {
                    rmdir(dirname($path));
                }
            }
        }
    }

    /**
     * Add nginx location files for the redirected domains.
     */
    protected function addRedirects()
    {
        foreach ($this->getRedirectFiles() as $url => $path) {
            $redirect = 'return 301 ' . $url . '\$request_uri;';
            if (!file_exists(dirname($path))) {
                mkdir(dirname($path), recursive: true);
            }
            file_put_contents($path, $redirect);
        }
    }

    protected function getRedirectFiles()
    {
        $config = $this->config;

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');

        $vhostsPath = 'shared/proxy-vhost.d';

        $files = [];
        $redirects = $this->config->get('environment.' . $activeEnvironment . '.routes.redirects') ?? [];
        foreach ($redirects as $vhostFrom => $routeTo) {
            $url = $protocol . '://' . $this->virtualHost->getVirtualHost($routeTo);
            // Generates a path like shared/proxy-vhost.d/www.example.com_location_replace.d/PROJECTNAME.conf
            // so projects that share the same virtual host don't delete each other redirects
            // when they are stopped/uninstalled.
            // TODO: Do not delete the files when they are in use.
            $files[$url] =
                $vhostsPath . '/' .
                $vhostFrom . '_location_replace.d/' .
                $this->dockerComposeService->getProjectName() . '.conf';
        }
        return $files;
    }
}

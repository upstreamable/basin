<?php

namespace Basin\Hook;

/**
 * Schedules commands to be executed after the container run.
 */
class PostRun
{
    /**
     * @param array<string> $commands
     */
    public function __construct(protected array $commands = [])
    {
    }

    /**
     * @return \Basin\Hook\PostRun
     */
    public function addCommand(string $command): PostRun
    {
        $this->commands[] = $command;
        return $this;
    }

    public function __destruct()
    {
        if (count($this->commands) > 0) {
            $script = implode(' && ', $this->commands);
            if (!file_exists('shared/tmp')) {
                mkdir('shared/tmp');
            }
            file_put_contents('shared/tmp/' . getenv('EXECUTION_ID') . '.post-run.sh', $script);
        }
    }
}

<?php

namespace Basin\Recipes\SSHVolume;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('docker.defaultContainer.user', 'sftp');
        $config->setDefault('docker.defaultContainer.name', 'web');
        $config->setDefault('docker.sshd-image.name', 'upstreamable/sshd');
        $config->setDefault('docker.sshd-image.composer-name', 'upstreamable/docker-sshd');

        $config->setDefault('passthroughCommands', [
            'composer',
        ]);
        $config->setDefault('composer.root', '.basin/');

        $config->setDefault('sshd.user', 'sftp');
        return $config;
    }
}

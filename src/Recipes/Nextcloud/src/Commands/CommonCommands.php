<?php

namespace Basin\Recipes\Nextcloud\Commands;

use Basin\Commands\RecipeBase;
use Basin\RecipeHelper\SMTP;
use Basin\RecipeHelper\MariaDb;
use Basin\RecipeHelper\Redis;

/**
 * Commands for a Nextcloud recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    use SMTP;
    use MariaDb;
    use Redis;

    public function occ(array $args)
    {
        $this->runPassThroughCommand('/var/www/html/occ', $args);
    }

    public function composer(array $args)
    {
        return $this
            ->taskExec('composer')
            ->args($args)
            ->dir('app/.basin')
            ->run();
    }

    public function cron()
    {
        $this->runPassThroughCommand('php', ['-f', '/var/www/html/cron.php']);
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.php-image.name') . ':' .
                        $composer->getLockedVersion($config->get('docker.php-image.composer-name')),
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'fastcgi-nextcloud',
                        'VIRTUAL_ROOT' => '/var/www/html',
                        'NEXTCLOUD_DATA_DIR' => '/var/www/data',
                        'NEXTCLOUD_TRUSTED_DOMAINS' => $virtualHost->getAllVirtualHosts(),
                        'NEXTCLOUD_ADMIN_USER' => $config->get('nextcloud.adminUser'),
                        'NEXTCLOUD_ADMIN_PASSWORD' => $config->get('nextcloud.adminPassword'),
                    ],
                    'volumes' => [
                        [
                          'type' => 'volume',
                          'source' => 'app',
                          'target' => '/var/www/html',
                        ],
                        [
                          'type' => 'volume',
                          'source' => 'data',
                          'target' => '/var/www/data',
                        ],
                    ],
                ],
                'wait-for-web' => [
                    'image' => 'dokku/wait:' . $lockedVersion->getVersion('wait'),
                    'restart' => 'no',
                    // Give the web container some time to install, update etc.
                    'command' => '-c web:9000 -t 120',
                    'depends_on' => [
                        'web' => ['condition' => 'service_started'],
                    ],
                ],
                'wait' => [
                    'image' => 'alpine:3.17',
                    'restart' => 'no',
                    'command' => 'echo all services started',
                    'depends_on' => [
                        'wait-for-web' => ['condition' => 'service_completed_successfully'],
                    ],
                ],
            ],
            'volumes' => [
                'app' => [
                    'external' => false,
                ],
                'data' => [
                    'external' => false,
                ],
            ],
        ];

        if ($config->get('db.type') === 'mariadb') {
            $dockerCompose = $this->addMariadb($dockerCompose, $config);
        }
        if ($config->get('cache.type') === 'redis') {
            $dockerCompose = $this->addRedis($dockerCompose, $config);
        }
        if ($config->get('mail.type') === 'smtp') {
            $dockerCompose = $this->addSMTP($dockerCompose, $config);
        }
        return $this->processDockerCompose($dockerCompose, $config);
    }
}

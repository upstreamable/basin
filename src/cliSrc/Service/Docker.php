<?php

namespace Basin\Service;

use Docker\API\Exception\ContainerInspectNotFoundException;
use Docker\API\Exception\NetworkInspectNotFoundException;
use Docker\API\Exception\VolumeDeleteNotFoundException;
use Docker\API\Exception\VolumeInspectNotFoundException;
use Docker\API\Model\ContainersCreatePostBody;
use Docker\API\Model\HostConfig;
use Docker\API\Model\Mount;
use Docker\API\Model\NetworksCreatePostBody;
use Docker\API\Model\VolumeCreateOptions;
use Docker\Docker as DockerClient;
use Docker\Stream\CallbackStream;

/**
 * Recipe-related methods.
 */
class Docker
{
    /**
     * @var \Docker\Docker
     */
    protected $client;
    protected $proxyContainerName;

    public function __construct()
    {
        $this->client = DockerClient::create();
        $this->proxyContainerName = 'basin-proxy-nginx-1';
    }

    /**
     * @return array<mixed>
     */
    public function getContainersWithPublishedPorts(array $ports): array
    {
        $containers = [];
        foreach ($ports as $port) {
            $query = [
                'filters' => json_encode([
                    'publish' => [$port],
                ]),
            ];
            $containers = array_merge($this->client->containerList($query), $containers);
        }

        return $containers;
    }

    /**
     * @return array<string>
     */
    public function getContainerNamesWithPublishedPorts(array $ports): array
    {
        $containers = $this->getContainersWithPublishedPorts($ports);
        $containerNames = [];
        foreach ($containers as $container) {
            $containerNames = array_merge($container->getNames(), $containerNames);
        }

        // Remove duplicates and the leading '/' for each name.
        $containerNames = array_map(function ($item) {
            return trim($item, '/');
        }, array_unique($containerNames));
        // Remove the proxy from the list.
        return array_diff($containerNames, [$this->proxyContainerName]);
    }

    /**
     * @return array<mixed>
     */
    public function getContainersAttachedToProxy(string $projectNameToIgnore = null): array
    {
        $containers = [];
        $query = [
            'filters' => json_encode([
                // All containers attached to the proxy have a recipe label.
                'label' => [getenv('LABEL_REVERSE_DNS') . '.recipe'],
                'status' => ['running'],
            ]),
        ];

        foreach ($this->client->containerList($query) as $container) {
            /** @var \ArrayObject */
            $labels = $container->getLabels();
            if (
                $projectNameToIgnore &&
                $labels->offsetGet('com.docker.compose.project') === $projectNameToIgnore
            ) {
                continue 1;
            }
            foreach ($container->getNames() as $containerName) {
                if (str_starts_with($containerName, '/basin-proxy')) {
                    continue 2;
                }
            }

            $containers[] = $container;
        }

        return $containers;
    }

    /**
     * @return array<mixed>
     */
    public function getContainersAttachedToProxyWithAllDetails(): array
    {
        $containers = [];
        foreach ($this->getContainersAttachedToProxy() as $container) {
            $containers[] = $this->client->containerInspect($container->getId());
        }

        return $containers;
    }

    /**
     * @return bool
     */
    public function isProxyStopped()
    {
        try {
            $this->client->containerInspect($this->proxyContainerName);
        } catch (ContainerInspectNotFoundException $e) {
            return true;
        }
        return false;
    }

    /**
     * @return string|bool
     */
    public function getEnvVariableFromContainer($container, string $variableName)
    {
        foreach ($container->getConfig()->getEnv() as $var) {
            if (str_starts_with($var, $variableName . '=')) {
                return substr($var, strlen($variableName) + 1);
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function volumeDelete($name): bool
    {
        try {
            $this->client->volumeDelete($name);
        } catch (VolumeDeleteNotFoundException $e) {
            return false;
        }
        return true;
    }

    public function volumeExists($volumeName): bool
    {
        try {
            $this->client->volumeInspect($volumeName);
        } catch (VolumeInspectNotFoundException $e) {
            return false;
        }
        return true;
    }

    public function createDockerComposeVolume($volumeName): bool
    {
        $parts = explode('_', $volumeName);
        $dockerComposeVolumeName = array_pop($parts);
        $dockerComposeProjectName = implode('_', $parts);
        $this->client->volumeCreate(
            (new VolumeCreateOptions())
            ->setName($volumeName)
            ->setLabels([
                'com.docker.compose.project' => $dockerComposeProjectName,
                'com.docker.compose.version' => getenv('DOCKERCOMPOSE_LOCK_VERSION'),
                'com.docker.compose.volume' => $dockerComposeVolumeName,
            ])
        );
        return true;
    }

    /**
     * @return bool
     */
    public function volumeClone($src, $dst): bool
    {
        if (!$this->volumeExists($src) || $this->volumeExists($dst)) {
            return false;
        }
        $imageName = 'alpine:' . trim(file_get_contents('/etc/alpine-release'));

        $buildStream = $this->client->imageCreate(queryParameters: ['fromImage' => $imageName]);
        if (!$buildStream instanceof CallbackStream) {
            return false;
        }
        $buildStream->wait();

        $this->createDockerComposeVolume($dst);

        $srcMount = (new Mount())
            ->setType('volume')
            ->setSource($src)
            ->setTarget('/source');

        $dstMount = (new Mount())
            ->setType('volume')
            ->setSource($dst)
            ->setTarget('/destination');

        $hostConfig = (new HostConfig())->setMounts([
            $srcMount,
            $dstMount,
        ]);

        $containerConfig = (new ContainersCreatePostBody())
            ->setHostConfig($hostConfig)
            ->setImage($imageName)
            ->setCmd(['sh', '-c', 'cd /source ; cp -av . /destination > /dev/null']);

        $containerCreateResult = $this->client->containerCreate($containerConfig);
        $this->client->containerStart($containerCreateResult->getId());
        $this->client->containerWait($containerCreateResult->getId());
        $statusCode = $this->client
            ->containerInspect($containerCreateResult->getId())
            ->getState()
            ->getExitCode();

        $this->client->containerDelete($containerCreateResult->getId());
        return $statusCode === 0;
    }

    public function networkExists($networkName): bool
    {
        try {
            $this->client->networkInspect($networkName);
        } catch (NetworkInspectNotFoundException $e) {
            return false;
        }
        return true;
    }

    /**
     * Mirror the naming and labels that docker-compose uses for a network.
     */
    public function createDockerComposeNetwork($networkName): bool
    {
        $parts = explode('_', $networkName);
        $dockerComposeNetworkName = array_pop($parts);
        $dockerComposeProjectName = implode('_', $parts);
        $this->client->networkCreate(
            (new NetworksCreatePostBody())
            ->setName($networkName)
            ->setLabels([
                'com.docker.compose.network' => $dockerComposeNetworkName,
                'com.docker.compose.project' => $dockerComposeProjectName,
                'com.docker.compose.version' => getenv('DOCKERCOMPOSE_LOCK_VERSION'),
            ])
        );
        return true;
    }
}

<?php

namespace Basin\Commands;

use Docker\Docker as DockerClient;
use Robo\ResultData;
use Robo\Tasks;

/**
 * Commands that are available when the recipe is known.
 *
 * @see http://robo.li/
 */
abstract class RecipeBase extends Tasks
{
    use Provision;

    abstract protected function getDockerCompose();

    /**
     * Run the application
     *
     * @aliases up
     */
    public function start()
    {
        $dockerCompose = $this->getDockerCompose();
        $dockerCompose = $this->addLabels($dockerCompose);
        return $this->doStart($dockerCompose);
    }

    /**
     * Stop the application
     */
    public function stop()
    {
        $dockerCompose = $this->getDockerCompose();
        return $this->doStop($dockerCompose);
    }

    /**
     * Stop the project and delete its data volumes.
     */
    public function uninstall($opts = ['removeImages' => false])
    {
        $dockerCompose = $this->getDockerCompose();
        return $this->doStop(
            $dockerCompose,
            removeVolumes: true,
            removeImages: $opts['removeImages'],
        );
    }

    /**
     * Deploy the app to an environment.
     *
     * @aliases deploy
     */
    public function deployRelease($environment = 'localhost')
    {
        $collection = $this->collectionBuilder();

        $config = $this->getContainer()->get('config');
        $deployHost = $config->get('environment.' . $environment . '.deploy.host');
        if (!$deployHost) {
            $this->say('No deploy host configured for the "' . $environment . '" environment');
            // TODO. Pass message to the user along with the ResultData.
            return new ResultData(ResultData::EXITCODE_ERROR);
        }

        $deployPath =
            $config->get('environment.' . $environment . '.deploy.path') .
            '/' .
            $config->get('projectName') . '-' . $environment;
        $currentReleasePath = $deployPath . '/current';

        $ansiblePlaybook = [[
            'name' => 'Deploy',
            'hosts' => 'all',
            'vars' => [
                'ansistrano_deploy_from' => "{{ lookup('env','HOME') }}/app/",
                'ansistrano_deploy_to' => $deployPath,
                'ansistrano_keep_releases' => $config->get(
                    'environment.' . $environment . '.ansistrano.keep_releases',
                    3
                ),
                'ansistrano_rsync_extra_params' => '--exclude-from=/etc/ansible/rsync-exclude',
                'ansistrano_allow_anonymous_stats' => $config->get('ansistrano.allow_anonymous_stats'),
                // Generated dynamically.
                'ansistrano_after_update_code_tasks_file' => '/tmp/after-update-code-tasks.yml',
                'ansistrano_before_cleanup_tasks_file' => '/etc/ansible/tasks/before-cleanup-tasks.yml',
            ],
            'roles' => [[
                'role' => 'ansistrano.deploy',
            ]],
        ]];
        if ($ansibleUser = $config->get('environment.' . $environment . '.deploy.user')) {
            $ansiblePlaybook[0]['vars']['ansible_user'] = $ansibleUser;
        }
        if ($ansibleUserPassword = $config->get('environment.' . $environment . '.deploy.password')) {
            $ansiblePlaybook[0]['vars']['ansible_password'] = $ansibleUserPassword;
        }

        $deployConfig = [
          'environment' => [
            'active' => $environment,
          ],
          'projectName' => $config->get('projectName'),
        ];
        $deployWrapperScript = '~/vendor/bin/wrapper';

        // When the deploy happens in the same host as the target environment, usually $environment = 'localhost',
        // A translation is needed betwen the container and the host path.
        // Additionally a different wrapper is needed to invoke basin cli from the
        // container.
        if ($pathOnDockerHost = $config->get('environment.' . $environment . '.deploy.pathOnDockerHost')) {
            $deployConfig['pathReplacement'] = [
                'from' => $config->get('environment.' . $environment . '.deploy.path'),
                'to' => $pathOnDockerHost,
            ];
            $deployWrapperScript = '~/vendor/bin/basin';
        }

        $afterUpdateCodeTasks = [];
        $afterUpdateCodeTasks[] = [
            'name' => 'Get status of current release',
            'stat' => [
                'path' => $currentReleasePath,
            ],
            'register' => 'current_release',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Stop current release',
            'shell' => './.basin/wrapper stop',
            'args' => [
                'chdir' => $currentReleasePath,
            ],
            'when' => 'current_release.stat.exists',
            'register' => 'stop_result',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Log the standard output of stopping the current release',
            'copy' => [
                'content' => '{{ stop_result.stdout }}',
                'dest' =>
                '{{ ansistrano_shared_path }}/{{ ansistrano_release_version }}.stop-current.stdout.log.txt',
            ],
            'when' => 'current_release.stat.exists',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Log the standard error output of stopping the current release',
            'copy' => [
                'content' => '{{ stop_result.stderr }}',
                'dest' =>
                '{{ ansistrano_shared_path }}/{{ ansistrano_release_version }}.stop-current.stderr.log.txt',
            ],
            'when' => 'current_release.stat.exists',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Copy wrapper script',
            'copy' => [
                'src' => $deployWrapperScript ,
                'dest' => '{{ ansistrano_release_path.stdout }}/.basin/wrapper',
                'mode' => 'preserve',
            ],
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Copy deploy configuration',
            'copy' => [
                'content' => '{{ item.content }}',
                'dest' => '{{ ansistrano_release_path.stdout }}/.basin/{{ item.dest }}',
                'mode' => 'u=rw,g=r,o=r',
            ],
            'loop' => [
                [
                    'content' => yaml_emit($deployConfig),
                    'dest' => 'deploy.yml'
                ],
                [
                    'content' => "{{ lookup('env', 'VERSION')}}",
                    'dest' => 'VERSION',
                ],
            ],
        ];

        if ($basinVersion = $config->get('environment.' . $environment . '.forceBasinVersion')) {
            $afterUpdateCodeTasks[] = [
                'name' => 'Pull the ' . $basinVersion . ' version image',
                'shell' => './.basin/wrapper self-update --local ' . $basinVersion,
                'args' => [
                    'chdir' => '{{ ansistrano_release_path.stdout }}',
                ]
            ];
        }

        $afterUpdateCodeTasks[] = [
            'name' => 'Copy volumes from the current release to the next one',
            'shell' => './.basin/wrapper deploy:copy-volumes {{ ansistrano_release_version }}',
            'args' => [
                'chdir' => $currentReleasePath,
            ],
            'when' => 'current_release.stat.exists',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Start the app',
            'shell' => './.basin/wrapper start',
            'args' => [
                'chdir' => '{{ ansistrano_release_path.stdout }}',
            ],
            'register' => 'start_result',
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Log the standard output of starting the app',
            'copy' => [
                'content' => '{{ start_result.stdout }}',
                'dest' => '{{ ansistrano_shared_path }}/{{ ansistrano_release_version }}.start.stdout.log.txt',
            ],
        ];
        $afterUpdateCodeTasks[] = [
            'name' => 'Log the error output of starting the app',
            'copy' => [
                'content' => '{{ start_result.stderr }}',
                'dest' => '{{ ansistrano_shared_path }}/{{ ansistrano_release_version }}.start.stderr.log.txt',
            ],
        ];
        if ($config->get('environment.' . $environment . '.cron')) {
            $ansibleCronConfig = [
                'name' => 'basin cron job for ' .  $config->get('projectName') . '-' . $environment,
                'job' => 'cd ' . $currentReleasePath . ' && ./.basin/wrapper ' .
                    $config->get('environment.' . $environment . '.cron.job'),
                'minute' => $config->get('environment.' . $environment . '.cron.minute'),
                'hour' => $config->get('environment.' . $environment . '.cron.hour'),
                'day' => $config->get('environment.' . $environment . '.cron.day'),
                'month' => $config->get('environment.' . $environment . '.cron.month'),
                'weekday' => $config->get('environment.' . $environment . '.cron.weekday'),
            ];

            $afterUpdateCodeTasks[] = [
                'name' => 'Install cron job on the host machine',
                'cron' => $ansibleCronConfig,
            ];
        }

        $collection->addTask(
            $this
                ->taskWriteToFile('/tmp/after-update-code-tasks.yml')
                ->text(yaml_emit($afterUpdateCodeTasks))
        );

        $collection->addTask($this->getAnsiblePlayBookTask($ansiblePlaybook, $deployHost));
        $result = $collection->run();
        if ($result->wasSuccessful()) {
            $virtualHost = $this->getContainer()->get('virtualhost');
            $protocol = $config->get('environment.' . $environment . '.http.protocol');
            $this->say(
                'The site can be accessed at ' .
                $protocol . '://' . $virtualHost->getVirtualHost('main', $environment),
            );
        }
        return $result;
    }

    /**
     * Copy all the persistent data volumes from this release to the next one.
     */
    public function deployCopyVolumes($nextRelease)
    {
        $docker = $this->getContainer()->get('docker');
        $dockerComposeService = $this->getContainer()->get('dockercompose');
        $config = $this->getContainer()->get('config');
        $nextRelease = $dockerComposeService->sanitize($nextRelease);

        $dockerCompose = $this->getDockerCompose();
        foreach ($dockerCompose['volumes'] ?? [] as $volumeName => $volumeDefinition) {
            if ($volumeDefinition['external']) {
                $source = $volumeDefinition['name'];
                $destination =  str_replace(
                    search: $dockerComposeService->getProjectName(),
                    replace: $config->get('cleanProjectName') . '-' . $nextRelease,
                    subject: $source
                );
            } else {
                $source = $dockerComposeService->getProjectName() . '_' . $volumeName;
                $destination = $config->get('cleanProjectName') . '-' . $nextRelease . '_' . $volumeName;
            }
            if ($docker->volumeClone($source, $destination)) {
                $this->say('Copied data from volume "' . $source . '" to volume "' . $destination . '".');
            } else {
                $this->say('Volumes can not be copied to the release "' . $nextRelease .
                    '". Do volume names already exist?');
                return new ResultData(ResultData::EXITCODE_ERROR);
            }
        }
    }

    /**
     * Uninstall all releases from an environment.
     */
    public function deployRemoveAll($environment = 'localhost')
    {
        $collection = $this->collectionBuilder();

        $config = $this->getContainer()->get('config');

        $deployHost = $config->get('environment.' . $environment . '.deploy.host');
        if (!$deployHost) {
            // TODO. Pass message to the user along with the ResultData.
            $this->say('No deploy host configured for the "' . $environment . '" environment');
            return new ResultData(ResultData::EXITCODE_ERROR);
        }

        $deployPath =
            $config->get('environment.' . $environment . '.deploy.path') .
            '/' .
            $config->get('projectName') . '-' . $environment;
        $releasesPath = $deployPath . '/releases';

        $tasks = [];
        if ($cronConfig = $config->get('environment.' . $environment . '.cron')) {
            $tasks[] = [
                'name' => 'Remove cron job',
                'cron' => [
                    'name' => 'basin cron job for ' .
                    $config->get('projectName') . '-' . $environment,
                    'state' => 'absent',
                ],
            ];
        }
        $tasks[] = [
            'name' => 'Releases directory is present',
            'file' => [
                'path' => $releasesPath,
                'state' => 'directory',
            ],
        ];
        $tasks[] = [
            'name' => 'Retrieve all releases',
            'command' => 'ls -v ' . $releasesPath,
            'register' => 'releases',
        ];
        $tasks[] = [
            'name' => 'Uninstall each release',
            'loop' => '{{ releases.stdout_lines }}',
            'shell' => './.basin/wrapper uninstall',
            'args' => ['chdir' => $releasesPath . '/{{ item }}'],
        ];
        $tasks[] = [
            'name' => 'Remove files',
            'file' => [
                'path' => $deployPath,
                'state' => 'absent',
            ],
        ];
        $ansiblePlaybook = [[
            'name' => 'Uninstall all releases for ' . $environment,
            'hosts' => 'all',
            'tasks' => $tasks,
        ]];

        $collection->addTask($this->getAnsiblePlayBookTask($ansiblePlaybook, $deployHost));

        $result = $collection->run();
        return $result;
    }

    protected function getAnsiblePlayBookTask($ansiblePlaybook, $deployHost)
    {
        $ansiblePlaybookTask = $this
            ->taskExec('ansible-playbook')
            ->setProcessInput(yaml_emit($ansiblePlaybook))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->arg('/dev/stdin');
        if ($deployHost !== 'localhost') {
            // Add a trailing comma to specify the inventory inline instead of a file.
            $ansiblePlaybookTask->option('inventory', $deployHost . ',');
        }
        return $ansiblePlaybookTask;
    }

    /**
     * Start the project.
     */
    protected function doStart($dockerCompose)
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');

        $dockerComposeProjectName = $this->getContainer()->get('dockercompose')->getProjectName();

        $docker = $this->getContainer()->get('docker');
        $this->proxyStart();
        // Do not proceed with the project if the first attemp failed to start the proxy.
        if ($docker->isProxyStopped()) {
            return;
        }

        $result = $this
            ->taskExec('docker-compose')
            ->setProcessInput(yaml_emit($dockerCompose))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->option('file', '/dev/stdin')
            // Explicit project name so it not guessed by docker-compose.
            ->option('project-name', $dockerComposeProjectName)
            ->arg('up')
            ->option('detach')
            ->option('remove-orphans')
            ->run();
        if ($result->wasSuccessful()) {
            $activeEnvironment = $config->get('environment.active');

            $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');
            $this->say(
                'The site can be accessed at ' .
                $protocol . '://' . $virtualHost->getVirtualHost(),
            );
        }
        return $result;
    }

    /**
     * Stop the project.
     */
    protected function doStop(
        $dockerCompose,
        $removeVolumes = false,
        $removeImages = false,
    ) {
        $collection = $this->collectionBuilder();
        $docker = $this->getContainer()->get('docker');

        $dockerComposeProjectName = $this->getContainer()->get('dockercompose')->getProjectName();

        $this->getContainer()->get('proxy')->projectIsStopping = true;
        if (count($docker->getContainersAttachedToProxy(projectNameToIgnore: $dockerComposeProjectName)) === 0) {
            $this->proxyStop();
            $this->say('No other projects running. Proxy has been stopped.');
        } else {
            // Restart proxy without the stopped proxy configuration.
            $this->proxyStart();
        }

        $task = $this
            ->taskExec('docker-compose')
            ->setProcessInput(yaml_emit($dockerCompose))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->option('file', '/dev/stdin')
            // Explicit project name so it not guessed by docker-compose.
            ->option('project-name', $dockerComposeProjectName)
            ->arg('down');

        if ($removeVolumes) {
            $task->option('volumes');
        }
        if ($removeImages) {
            $task->option('rmi', 'all');
        }

        $collection->addTask($task);

        return $collection->run();
    }

    /**
     * Start the proxy.
     */
    protected function proxyStart()
    {
        $config = $this->getContainer()->get('config');
        $proxy = $this->getContainer()->get('proxy');
        $httpPort = $config->get('proxy.http_port');
        $httpsPort = $config->get('proxy.https_port');

        // Ensure there are no other containers grabbing the ports and,
        // in case there are, attemp to stop them.
        $docker = $this->getContainer()->get('docker');
        $containersUsingPorts = $docker->getContainerNamesWithPublishedPorts([$httpPort, $httpsPort]);
        if ($containersUsingPorts) {
            $exit = $this->askForProxyStop($containersUsingPorts, [$httpPort, $httpsPort]);
            if ($exit) {
                return;
            }
        }

        $dockerCompose = $proxy->getDockerCompose();

        $result = $this
            ->taskExec('docker-compose')
            ->setProcessInput(yaml_emit($dockerCompose))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->option('file', '/dev/stdin')
            ->option('project-name', 'basin-proxy')
            ->arg('up')
            ->option('detach')
            ->run();
        return $result;
    }

    /**
     * Stop the proxy.
     */
    protected function proxyStop()
    {
        $proxy = $this->getContainer()->get('proxy');
        $dockerCompose = $proxy->getDockerCompose();

        $result = $this
            ->taskExec('docker-compose')
            ->setProcessInput(yaml_emit($dockerCompose))
            // The input is provided above so avoid getting it from other inputs.
            ->interactive(false)
            ->option('file', '/dev/stdin')
            ->option('project-name', 'basin-proxy')
            // Destroy containers and networks, preserve volumes.
            ->arg('down')
            ->run();
        return $result;
    }

    /**
     * Ask the user to decide about other containers blocking ports.
     *
     * @return bool
     *   True if the execution should be stopped here for another retry.
     */
    protected function askForProxyStop($containersUsingPorts, $ports)
    {
        $postRun = $this->getContainer()->get('postrun');
        if (
            in_array('amazeeio-haproxy', $containersUsingPorts, true) &&
            $this->confirm(
                'pygmy is running and using the following ports: ' .
                implode(', ', $ports) . '. Stop it?',
                true
            )
        ) {
            $postRun->addCommand('echo "Stopping pygmy"');
            $postRun->addCommand('pygmy stop');
            $postRun->addCommand('basin "$@"');
            return true;
        } elseif (
            $this->confirm('There are docker containers (' . implode(', ', $containersUsingPorts) .
            ') keeping the basin proxy from using the following ports: ' .
            implode(', ', $ports) .
            '. Stop them?')
        ) {
            $docker = DockerClient::create();
            foreach ($containersUsingPorts as $containerName) {
                $docker->containerStop($containerName);
            }

            // With the containers stopped the proxy should be able to start.
            return false;
        }
        $this->io()->error(
            'The proxy can not be started. Please stop the services using the ports before trying again.'
        );
        return true;
    }


    /**
     * Add labels for the project.
     */
    protected function addLabels($dockerCompose)
    {
        $dockerCompose['services']['web']['labels'][getenv('LABEL_REVERSE_DNS') . '.host-working-dir'] =
            getenv('HOST_WORKING_DIR');
        $dockerCompose['services']['web']['labels'][getenv('LABEL_REVERSE_DNS') . '.recipe'] =
            $this->getContainer()->get('recipes')->currentRecipe;
        return $dockerCompose;
    }

    protected function runPassThroughCommand(string $command, array $args)
    {
        $postRun = $this->getContainer()->get('postrun');
        $config = $this->getContainer()->get('config');

        $dockerComposeProjectName = $this->getContainer()->get('dockercompose')->getProjectName();

        $args = array_map([$this, 'stringWrap'], $args);

        // TODO: Replace for a call to the docker API.
        $postRun->addCommand('docker exec --interactive ' . getenv('ATTACH_TTY') .
            ' --user ' . $config->get('docker.defaultContainer.user') .
            ' ' . $dockerComposeProjectName . '-' . $config->get('docker.defaultContainer.name') . '-1 ' .
            $command . ' ' . implode(' ', $args));
    }

    /**
     * Surround a string with a character.
     */
    protected function stringWrap($string = '', $char = '"')
    {
        return str_pad($string, strlen($string) + 2, $char, STR_PAD_BOTH);
    }

    /**
     * Configure restart for services and fpm vars.
     */
    protected function processDockerCompose($dockerCompose, $config)
    {
        if ($extraEnvVars = $config->get('docker.defaultContainer.environment')) {
            $dockerCompose['services']['web']['environment'] = array_merge(
                $dockerCompose['services']['web']['environment'],
                $extraEnvVars
            );
            $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'] =
                array_merge(
                    $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'] ?? [],
                    array_keys($extraEnvVars)
                );
        }

        $activeEnvironment = $config->get('environment.active');

        $restart = $config->get('environment.' . $activeEnvironment . '.service.restart');

        foreach (array_keys($dockerCompose['services']) as $serviceId) {
            $dockerCompose['services'][$serviceId]['restart'] =
                $dockerCompose['services'][$serviceId]['restart'] ??
                $restart;
        }
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'] =
            json_encode($dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'] ?? []);
        return $dockerCompose;
    }
}

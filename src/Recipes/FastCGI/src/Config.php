<?php

namespace Basin\Recipes\FastCGI;

class Config
{
    /**
     * Set config defaults.
     *
     * @param \Consolidation\Config\ConfigInterface $config;
     *
     * @return \Consolidation\Config\ConfigInterface
     */
    public static function setConfigDefaults($config)
    {
        $config->setDefault('docker.defaultContainer.user', 'www-data');
        $config->setDefault('docker.defaultContainer.name', 'web');

        $config->setDefault('passthroughCommands', [
            'composer',
        ]);
        $config->setDefault('composer.root', '.basin/');

        return $config;
    }
}
